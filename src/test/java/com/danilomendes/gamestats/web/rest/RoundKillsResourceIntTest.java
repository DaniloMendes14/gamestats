package com.danilomendes.gamestats.web.rest;

import com.danilomendes.gamestats.GamestatsApp;

import com.danilomendes.gamestats.domain.RoundKills;
import com.danilomendes.gamestats.repository.RoundKillsRepository;
import com.danilomendes.gamestats.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.danilomendes.gamestats.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RoundKillsResource REST controller.
 *
 * @see RoundKillsResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GamestatsApp.class)
public class RoundKillsResourceIntTest {

    private static final Long DEFAULT_NO_KILL = 1L;
    private static final Long UPDATED_NO_KILL = 2L;

    private static final Long DEFAULT_ONE_KILL = 1L;
    private static final Long UPDATED_ONE_KILL = 2L;

    private static final Long DEFAULT_TWO_KILLS = 1L;
    private static final Long UPDATED_TWO_KILLS = 2L;

    private static final Long DEFAULT_THREE_KILLS = 1L;
    private static final Long UPDATED_THREE_KILLS = 2L;

    private static final Long DEFAULT_FOUR_KILLS = 1L;
    private static final Long UPDATED_FOUR_KILLS = 2L;

    private static final Long DEFAULT_FIVE_KILLS = 1L;
    private static final Long UPDATED_FIVE_KILLS = 2L;

    @Autowired
    private RoundKillsRepository roundKillsRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRoundKillsMockMvc;

    private RoundKills roundKills;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RoundKillsResource roundKillsResource = new RoundKillsResource(roundKillsRepository);
        this.restRoundKillsMockMvc = MockMvcBuilders.standaloneSetup(roundKillsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RoundKills createEntity(EntityManager em) {
        RoundKills roundKills = new RoundKills()
            .noKill(DEFAULT_NO_KILL)
            .oneKill(DEFAULT_ONE_KILL)
            .twoKills(DEFAULT_TWO_KILLS)
            .threeKills(DEFAULT_THREE_KILLS)
            .fourKills(DEFAULT_FOUR_KILLS)
            .fiveKills(DEFAULT_FIVE_KILLS);
        return roundKills;
    }

    @Before
    public void initTest() {
        roundKills = createEntity(em);
    }

    @Test
    @Transactional
    public void createRoundKills() throws Exception {
        int databaseSizeBeforeCreate = roundKillsRepository.findAll().size();

        // Create the RoundKills
        restRoundKillsMockMvc.perform(post("/api/round-kills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roundKills)))
            .andExpect(status().isCreated());

        // Validate the RoundKills in the database
        List<RoundKills> roundKillsList = roundKillsRepository.findAll();
        assertThat(roundKillsList).hasSize(databaseSizeBeforeCreate + 1);
        RoundKills testRoundKills = roundKillsList.get(roundKillsList.size() - 1);
        assertThat(testRoundKills.getNoKill()).isEqualTo(DEFAULT_NO_KILL);
        assertThat(testRoundKills.getOneKill()).isEqualTo(DEFAULT_ONE_KILL);
        assertThat(testRoundKills.getTwoKills()).isEqualTo(DEFAULT_TWO_KILLS);
        assertThat(testRoundKills.getThreeKills()).isEqualTo(DEFAULT_THREE_KILLS);
        assertThat(testRoundKills.getFourKills()).isEqualTo(DEFAULT_FOUR_KILLS);
        assertThat(testRoundKills.getFiveKills()).isEqualTo(DEFAULT_FIVE_KILLS);
    }

    @Test
    @Transactional
    public void createRoundKillsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = roundKillsRepository.findAll().size();

        // Create the RoundKills with an existing ID
        roundKills.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRoundKillsMockMvc.perform(post("/api/round-kills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roundKills)))
            .andExpect(status().isBadRequest());

        // Validate the RoundKills in the database
        List<RoundKills> roundKillsList = roundKillsRepository.findAll();
        assertThat(roundKillsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRoundKills() throws Exception {
        // Initialize the database
        roundKillsRepository.saveAndFlush(roundKills);

        // Get all the roundKillsList
        restRoundKillsMockMvc.perform(get("/api/round-kills?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(roundKills.getId().intValue())))
            .andExpect(jsonPath("$.[*].noKill").value(hasItem(DEFAULT_NO_KILL.intValue())))
            .andExpect(jsonPath("$.[*].oneKill").value(hasItem(DEFAULT_ONE_KILL.intValue())))
            .andExpect(jsonPath("$.[*].twoKills").value(hasItem(DEFAULT_TWO_KILLS.intValue())))
            .andExpect(jsonPath("$.[*].threeKills").value(hasItem(DEFAULT_THREE_KILLS.intValue())))
            .andExpect(jsonPath("$.[*].fourKills").value(hasItem(DEFAULT_FOUR_KILLS.intValue())))
            .andExpect(jsonPath("$.[*].fiveKills").value(hasItem(DEFAULT_FIVE_KILLS.intValue())));
    }

    @Test
    @Transactional
    public void getRoundKills() throws Exception {
        // Initialize the database
        roundKillsRepository.saveAndFlush(roundKills);

        // Get the roundKills
        restRoundKillsMockMvc.perform(get("/api/round-kills/{id}", roundKills.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(roundKills.getId().intValue()))
            .andExpect(jsonPath("$.noKill").value(DEFAULT_NO_KILL.intValue()))
            .andExpect(jsonPath("$.oneKill").value(DEFAULT_ONE_KILL.intValue()))
            .andExpect(jsonPath("$.twoKills").value(DEFAULT_TWO_KILLS.intValue()))
            .andExpect(jsonPath("$.threeKills").value(DEFAULT_THREE_KILLS.intValue()))
            .andExpect(jsonPath("$.fourKills").value(DEFAULT_FOUR_KILLS.intValue()))
            .andExpect(jsonPath("$.fiveKills").value(DEFAULT_FIVE_KILLS.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingRoundKills() throws Exception {
        // Get the roundKills
        restRoundKillsMockMvc.perform(get("/api/round-kills/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRoundKills() throws Exception {
        // Initialize the database
        roundKillsRepository.saveAndFlush(roundKills);
        int databaseSizeBeforeUpdate = roundKillsRepository.findAll().size();

        // Update the roundKills
        RoundKills updatedRoundKills = roundKillsRepository.findOne(roundKills.getId());
        // Disconnect from session so that the updates on updatedRoundKills are not directly saved in db
        em.detach(updatedRoundKills);
        updatedRoundKills
            .noKill(UPDATED_NO_KILL)
            .oneKill(UPDATED_ONE_KILL)
            .twoKills(UPDATED_TWO_KILLS)
            .threeKills(UPDATED_THREE_KILLS)
            .fourKills(UPDATED_FOUR_KILLS)
            .fiveKills(UPDATED_FIVE_KILLS);

        restRoundKillsMockMvc.perform(put("/api/round-kills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRoundKills)))
            .andExpect(status().isOk());

        // Validate the RoundKills in the database
        List<RoundKills> roundKillsList = roundKillsRepository.findAll();
        assertThat(roundKillsList).hasSize(databaseSizeBeforeUpdate);
        RoundKills testRoundKills = roundKillsList.get(roundKillsList.size() - 1);
        assertThat(testRoundKills.getNoKill()).isEqualTo(UPDATED_NO_KILL);
        assertThat(testRoundKills.getOneKill()).isEqualTo(UPDATED_ONE_KILL);
        assertThat(testRoundKills.getTwoKills()).isEqualTo(UPDATED_TWO_KILLS);
        assertThat(testRoundKills.getThreeKills()).isEqualTo(UPDATED_THREE_KILLS);
        assertThat(testRoundKills.getFourKills()).isEqualTo(UPDATED_FOUR_KILLS);
        assertThat(testRoundKills.getFiveKills()).isEqualTo(UPDATED_FIVE_KILLS);
    }

    @Test
    @Transactional
    public void updateNonExistingRoundKills() throws Exception {
        int databaseSizeBeforeUpdate = roundKillsRepository.findAll().size();

        // Create the RoundKills

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRoundKillsMockMvc.perform(put("/api/round-kills")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(roundKills)))
            .andExpect(status().isCreated());

        // Validate the RoundKills in the database
        List<RoundKills> roundKillsList = roundKillsRepository.findAll();
        assertThat(roundKillsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRoundKills() throws Exception {
        // Initialize the database
        roundKillsRepository.saveAndFlush(roundKills);
        int databaseSizeBeforeDelete = roundKillsRepository.findAll().size();

        // Get the roundKills
        restRoundKillsMockMvc.perform(delete("/api/round-kills/{id}", roundKills.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RoundKills> roundKillsList = roundKillsRepository.findAll();
        assertThat(roundKillsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RoundKills.class);
        RoundKills roundKills1 = new RoundKills();
        roundKills1.setId(1L);
        RoundKills roundKills2 = new RoundKills();
        roundKills2.setId(roundKills1.getId());
        assertThat(roundKills1).isEqualTo(roundKills2);
        roundKills2.setId(2L);
        assertThat(roundKills1).isNotEqualTo(roundKills2);
        roundKills1.setId(null);
        assertThat(roundKills1).isNotEqualTo(roundKills2);
    }
}
