package com.danilomendes.gamestats.web.rest;

import com.danilomendes.gamestats.GamestatsApp;

import com.danilomendes.gamestats.domain.CSGOTeamUrl;
import com.danilomendes.gamestats.repository.CSGOTeamUrlRepository;
import com.danilomendes.gamestats.service.CSGOTeamUrlService;
import com.danilomendes.gamestats.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.danilomendes.gamestats.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CSGOTeamUrlResource REST controller.
 *
 * @see CSGOTeamUrlResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GamestatsApp.class)
public class CSGOTeamUrlResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_STATS_URL = "AAAAAAAAAA";
    private static final String UPDATED_STATS_URL = "BBBBBBBBBB";

    private static final String DEFAULT_PHOTO_URL = "AAAAAAAAAA";
    private static final String UPDATED_PHOTO_URL = "BBBBBBBBBB";

    private static final Long DEFAULT_TEAM_ID = 1L;
    private static final Long UPDATED_TEAM_ID = 2L;

    @Autowired
    private CSGOTeamUrlRepository cSGOTeamUrlRepository;

    @Autowired
    private CSGOTeamUrlService cSGOTeamUrlService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCSGOTeamUrlMockMvc;

    private CSGOTeamUrl cSGOTeamUrl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CSGOTeamUrlResource cSGOTeamUrlResource = new CSGOTeamUrlResource(cSGOTeamUrlService);
        this.restCSGOTeamUrlMockMvc = MockMvcBuilders.standaloneSetup(cSGOTeamUrlResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CSGOTeamUrl createEntity(EntityManager em) {
        CSGOTeamUrl cSGOTeamUrl = new CSGOTeamUrl()
            .name(DEFAULT_NAME)
            .statsUrl(DEFAULT_STATS_URL)
            .photoUrl(DEFAULT_PHOTO_URL)
            .teamId(DEFAULT_TEAM_ID);
        return cSGOTeamUrl;
    }

    @Before
    public void initTest() {
        cSGOTeamUrl = createEntity(em);
    }

    @Test
    @Transactional
    public void createCSGOTeamUrl() throws Exception {
        int databaseSizeBeforeCreate = cSGOTeamUrlRepository.findAll().size();

        // Create the CSGOTeamUrl
        restCSGOTeamUrlMockMvc.perform(post("/api/csgo-team-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOTeamUrl)))
            .andExpect(status().isCreated());

        // Validate the CSGOTeamUrl in the database
        List<CSGOTeamUrl> cSGOTeamUrlList = cSGOTeamUrlRepository.findAll();
        assertThat(cSGOTeamUrlList).hasSize(databaseSizeBeforeCreate + 1);
        CSGOTeamUrl testCSGOTeamUrl = cSGOTeamUrlList.get(cSGOTeamUrlList.size() - 1);
        assertThat(testCSGOTeamUrl.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCSGOTeamUrl.getStatsUrl()).isEqualTo(DEFAULT_STATS_URL);
        assertThat(testCSGOTeamUrl.getPhotoUrl()).isEqualTo(DEFAULT_PHOTO_URL);
        assertThat(testCSGOTeamUrl.getTeamId()).isEqualTo(DEFAULT_TEAM_ID);
    }

    @Test
    @Transactional
    public void createCSGOTeamUrlWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cSGOTeamUrlRepository.findAll().size();

        // Create the CSGOTeamUrl with an existing ID
        cSGOTeamUrl.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCSGOTeamUrlMockMvc.perform(post("/api/csgo-team-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOTeamUrl)))
            .andExpect(status().isBadRequest());

        // Validate the CSGOTeamUrl in the database
        List<CSGOTeamUrl> cSGOTeamUrlList = cSGOTeamUrlRepository.findAll();
        assertThat(cSGOTeamUrlList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCSGOTeamUrls() throws Exception {
        // Initialize the database
        cSGOTeamUrlRepository.saveAndFlush(cSGOTeamUrl);

        // Get all the cSGOTeamUrlList
        restCSGOTeamUrlMockMvc.perform(get("/api/csgo-team-urls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cSGOTeamUrl.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].statsUrl").value(hasItem(DEFAULT_STATS_URL.toString())))
            .andExpect(jsonPath("$.[*].photoUrl").value(hasItem(DEFAULT_PHOTO_URL.toString())))
            .andExpect(jsonPath("$.[*].teamId").value(hasItem(DEFAULT_TEAM_ID.intValue())));
    }

    @Test
    @Transactional
    public void getCSGOTeamUrl() throws Exception {
        // Initialize the database
        cSGOTeamUrlRepository.saveAndFlush(cSGOTeamUrl);

        // Get the cSGOTeamUrl
        restCSGOTeamUrlMockMvc.perform(get("/api/csgo-team-urls/{id}", cSGOTeamUrl.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cSGOTeamUrl.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.statsUrl").value(DEFAULT_STATS_URL.toString()))
            .andExpect(jsonPath("$.photoUrl").value(DEFAULT_PHOTO_URL.toString()))
            .andExpect(jsonPath("$.teamId").value(DEFAULT_TEAM_ID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCSGOTeamUrl() throws Exception {
        // Get the cSGOTeamUrl
        restCSGOTeamUrlMockMvc.perform(get("/api/csgo-team-urls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCSGOTeamUrl() throws Exception {
        // Initialize the database
        cSGOTeamUrlService.save(cSGOTeamUrl);

        int databaseSizeBeforeUpdate = cSGOTeamUrlRepository.findAll().size();

        // Update the cSGOTeamUrl
        CSGOTeamUrl updatedCSGOTeamUrl = cSGOTeamUrlRepository.findOne(cSGOTeamUrl.getId());
        // Disconnect from session so that the updates on updatedCSGOTeamUrl are not directly saved in db
        em.detach(updatedCSGOTeamUrl);
        updatedCSGOTeamUrl
            .name(UPDATED_NAME)
            .statsUrl(UPDATED_STATS_URL)
            .photoUrl(UPDATED_PHOTO_URL)
            .teamId(UPDATED_TEAM_ID);

        restCSGOTeamUrlMockMvc.perform(put("/api/csgo-team-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCSGOTeamUrl)))
            .andExpect(status().isOk());

        // Validate the CSGOTeamUrl in the database
        List<CSGOTeamUrl> cSGOTeamUrlList = cSGOTeamUrlRepository.findAll();
        assertThat(cSGOTeamUrlList).hasSize(databaseSizeBeforeUpdate);
        CSGOTeamUrl testCSGOTeamUrl = cSGOTeamUrlList.get(cSGOTeamUrlList.size() - 1);
        assertThat(testCSGOTeamUrl.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCSGOTeamUrl.getStatsUrl()).isEqualTo(UPDATED_STATS_URL);
        assertThat(testCSGOTeamUrl.getPhotoUrl()).isEqualTo(UPDATED_PHOTO_URL);
        assertThat(testCSGOTeamUrl.getTeamId()).isEqualTo(UPDATED_TEAM_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingCSGOTeamUrl() throws Exception {
        int databaseSizeBeforeUpdate = cSGOTeamUrlRepository.findAll().size();

        // Create the CSGOTeamUrl

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCSGOTeamUrlMockMvc.perform(put("/api/csgo-team-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOTeamUrl)))
            .andExpect(status().isCreated());

        // Validate the CSGOTeamUrl in the database
        List<CSGOTeamUrl> cSGOTeamUrlList = cSGOTeamUrlRepository.findAll();
        assertThat(cSGOTeamUrlList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCSGOTeamUrl() throws Exception {
        // Initialize the database
        cSGOTeamUrlService.save(cSGOTeamUrl);

        int databaseSizeBeforeDelete = cSGOTeamUrlRepository.findAll().size();

        // Get the cSGOTeamUrl
        restCSGOTeamUrlMockMvc.perform(delete("/api/csgo-team-urls/{id}", cSGOTeamUrl.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CSGOTeamUrl> cSGOTeamUrlList = cSGOTeamUrlRepository.findAll();
        assertThat(cSGOTeamUrlList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CSGOTeamUrl.class);
        CSGOTeamUrl cSGOTeamUrl1 = new CSGOTeamUrl();
        cSGOTeamUrl1.setId(1L);
        CSGOTeamUrl cSGOTeamUrl2 = new CSGOTeamUrl();
        cSGOTeamUrl2.setId(cSGOTeamUrl1.getId());
        assertThat(cSGOTeamUrl1).isEqualTo(cSGOTeamUrl2);
        cSGOTeamUrl2.setId(2L);
        assertThat(cSGOTeamUrl1).isNotEqualTo(cSGOTeamUrl2);
        cSGOTeamUrl1.setId(null);
        assertThat(cSGOTeamUrl1).isNotEqualTo(cSGOTeamUrl2);
    }
}
