package com.danilomendes.gamestats.web.rest;

import com.danilomendes.gamestats.GamestatsApp;

import com.danilomendes.gamestats.domain.CSGOPlayer;
import com.danilomendes.gamestats.repository.CSGOPlayerRepository;
import com.danilomendes.gamestats.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.util.List;

import static com.danilomendes.gamestats.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CSGOPlayerResource REST controller.
 *
 * @see CSGOPlayerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GamestatsApp.class)
public class CSGOPlayerResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_AGE = 1L;
    private static final Long UPDATED_AGE = 2L;

    private static final String DEFAULT_NICK = "AAAAAAAAAA";
    private static final String UPDATED_NICK = "BBBBBBBBBB";

    private static final Double DEFAULT_RATING = 1D;
    private static final Double UPDATED_RATING = 2D;

    private static final Double DEFAULT_USERS_RATING = 1D;
    private static final Double UPDATED_USERS_RATING = 2D;

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final Long DEFAULT_MAPS_PLAYED = 1L;
    private static final Long UPDATED_MAPS_PLAYED = 2L;

    private static final Double DEFAULT_WIN_LOSS_RATIO = 1D;
    private static final Double UPDATED_WIN_LOSS_RATIO = 2D;

    private static final Double DEFAULT_T_WIN_RATIO = 1D;
    private static final Double UPDATED_T_WIN_RATIO = 2D;

    private static final Double DEFAULT_CT_WIN_RATIO = 1D;
    private static final Double UPDATED_CT_WIN_RATIO = 2D;

    private static final Double DEFAULT_KD_RATIO = 1D;
    private static final Double UPDATED_KD_RATIO = 2D;

    private static final Long DEFAULT_KD_DIFF = 1L;
    private static final Long UPDATED_KD_DIFF = 2L;

    private static final Double DEFAULT_HEADSHOT_RADIO = 1D;
    private static final Double UPDATED_HEADSHOT_RADIO = 2D;

    private static final String DEFAULT_BEST_WEAPON = "AAAAAAAAAA";
    private static final String UPDATED_BEST_WEAPON = "BBBBBBBBBB";

    private static final String DEFAULT_TWITTER_URL = "AAAAAAAAAA";
    private static final String UPDATED_TWITTER_URL = "BBBBBBBBBB";

    private static final String DEFAULT_TWITCH_URL = "AAAAAAAAAA";
    private static final String UPDATED_TWITCH_URL = "BBBBBBBBBB";

    private static final String DEFAULT_HARDWARE_SPECS = "AAAAAAAAAA";
    private static final String UPDATED_HARDWARE_SPECS = "BBBBBBBBBB";

    private static final Long DEFAULT_TOTAL_ROUNDS = 1L;
    private static final Long UPDATED_TOTAL_ROUNDS = 2L;

    private static final Double DEFAULT_GRENADE_DMG_ROUND = 1D;
    private static final Double UPDATED_GRENADE_DMG_ROUND = 2D;

    private static final Double DEFAULT_KILL_ROUND_RATIO = 1D;
    private static final Double UPDATED_KILL_ROUND_RATIO = 2D;

    private static final Double DEFAULT_DEATH_ROUND_RATIO = 1D;
    private static final Double UPDATED_DEATH_ROUND_RATIO = 2D;

    @Autowired
    private CSGOPlayerRepository cSGOPlayerRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCSGOPlayerMockMvc;

    private CSGOPlayer cSGOPlayer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CSGOPlayerResource cSGOPlayerResource = new CSGOPlayerResource(cSGOPlayerRepository);
        this.restCSGOPlayerMockMvc = MockMvcBuilders.standaloneSetup(cSGOPlayerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CSGOPlayer createEntity(EntityManager em) {
        CSGOPlayer cSGOPlayer = new CSGOPlayer()
            .name(DEFAULT_NAME)
            .age(DEFAULT_AGE)
            .nick(DEFAULT_NICK)
            .rating(DEFAULT_RATING)
            .usersRating(DEFAULT_USERS_RATING)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE)
            .mapsPlayed(DEFAULT_MAPS_PLAYED)
            .winLossRatio(DEFAULT_WIN_LOSS_RATIO)
            .tWinRatio(DEFAULT_T_WIN_RATIO)
            .ctWinRatio(DEFAULT_CT_WIN_RATIO)
            .kdRatio(DEFAULT_KD_RATIO)
            .kdDiff(DEFAULT_KD_DIFF)
            .headshotRadio(DEFAULT_HEADSHOT_RADIO)
            .bestWeapon(DEFAULT_BEST_WEAPON)
            .twitterUrl(DEFAULT_TWITTER_URL)
            .twitchUrl(DEFAULT_TWITCH_URL)
            .hardwareSpecs(DEFAULT_HARDWARE_SPECS)
            .totalRounds(DEFAULT_TOTAL_ROUNDS)
            .grenadeDmgRound(DEFAULT_GRENADE_DMG_ROUND)
            .killRoundRatio(DEFAULT_KILL_ROUND_RATIO)
            .deathRoundRatio(DEFAULT_DEATH_ROUND_RATIO);
        return cSGOPlayer;
    }

    @Before
    public void initTest() {
        cSGOPlayer = createEntity(em);
    }

    @Test
    @Transactional
    public void createCSGOPlayer() throws Exception {
        int databaseSizeBeforeCreate = cSGOPlayerRepository.findAll().size();

        // Create the CSGOPlayer
        restCSGOPlayerMockMvc.perform(post("/api/csgo-players")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOPlayer)))
            .andExpect(status().isCreated());

        // Validate the CSGOPlayer in the database
        List<CSGOPlayer> cSGOPlayerList = cSGOPlayerRepository.findAll();
        assertThat(cSGOPlayerList).hasSize(databaseSizeBeforeCreate + 1);
        CSGOPlayer testCSGOPlayer = cSGOPlayerList.get(cSGOPlayerList.size() - 1);
        assertThat(testCSGOPlayer.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCSGOPlayer.getAge()).isEqualTo(DEFAULT_AGE);
        assertThat(testCSGOPlayer.getNick()).isEqualTo(DEFAULT_NICK);
        assertThat(testCSGOPlayer.getRating()).isEqualTo(DEFAULT_RATING);
        assertThat(testCSGOPlayer.getUsersRating()).isEqualTo(DEFAULT_USERS_RATING);
        assertThat(testCSGOPlayer.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testCSGOPlayer.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testCSGOPlayer.getMapsPlayed()).isEqualTo(DEFAULT_MAPS_PLAYED);
        assertThat(testCSGOPlayer.getWinLossRatio()).isEqualTo(DEFAULT_WIN_LOSS_RATIO);
        assertThat(testCSGOPlayer.gettWinRatio()).isEqualTo(DEFAULT_T_WIN_RATIO);
        assertThat(testCSGOPlayer.getCtWinRatio()).isEqualTo(DEFAULT_CT_WIN_RATIO);
        assertThat(testCSGOPlayer.getKdRatio()).isEqualTo(DEFAULT_KD_RATIO);
        assertThat(testCSGOPlayer.getKdDiff()).isEqualTo(DEFAULT_KD_DIFF);
        assertThat(testCSGOPlayer.getHeadshotRadio()).isEqualTo(DEFAULT_HEADSHOT_RADIO);
        assertThat(testCSGOPlayer.getBestWeapon()).isEqualTo(DEFAULT_BEST_WEAPON);
        assertThat(testCSGOPlayer.getTwitterUrl()).isEqualTo(DEFAULT_TWITTER_URL);
        assertThat(testCSGOPlayer.getTwitchUrl()).isEqualTo(DEFAULT_TWITCH_URL);
        assertThat(testCSGOPlayer.getHardwareSpecs()).isEqualTo(DEFAULT_HARDWARE_SPECS);
        assertThat(testCSGOPlayer.getTotalRounds()).isEqualTo(DEFAULT_TOTAL_ROUNDS);
        assertThat(testCSGOPlayer.getGrenadeDmgRound()).isEqualTo(DEFAULT_GRENADE_DMG_ROUND);
        assertThat(testCSGOPlayer.getKillRoundRatio()).isEqualTo(DEFAULT_KILL_ROUND_RATIO);
        assertThat(testCSGOPlayer.getDeathRoundRatio()).isEqualTo(DEFAULT_DEATH_ROUND_RATIO);
    }

    @Test
    @Transactional
    public void createCSGOPlayerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cSGOPlayerRepository.findAll().size();

        // Create the CSGOPlayer with an existing ID
        cSGOPlayer.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCSGOPlayerMockMvc.perform(post("/api/csgo-players")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOPlayer)))
            .andExpect(status().isBadRequest());

        // Validate the CSGOPlayer in the database
        List<CSGOPlayer> cSGOPlayerList = cSGOPlayerRepository.findAll();
        assertThat(cSGOPlayerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCSGOPlayers() throws Exception {
        // Initialize the database
        cSGOPlayerRepository.saveAndFlush(cSGOPlayer);

        // Get all the cSGOPlayerList
        restCSGOPlayerMockMvc.perform(get("/api/csgo-players?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cSGOPlayer.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].age").value(hasItem(DEFAULT_AGE.intValue())))
            .andExpect(jsonPath("$.[*].nick").value(hasItem(DEFAULT_NICK.toString())))
            .andExpect(jsonPath("$.[*].rating").value(hasItem(DEFAULT_RATING.doubleValue())))
            .andExpect(jsonPath("$.[*].usersRating").value(hasItem(DEFAULT_USERS_RATING.doubleValue())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].mapsPlayed").value(hasItem(DEFAULT_MAPS_PLAYED.intValue())))
            .andExpect(jsonPath("$.[*].winLossRatio").value(hasItem(DEFAULT_WIN_LOSS_RATIO.doubleValue())))
            .andExpect(jsonPath("$.[*].tWinRatio").value(hasItem(DEFAULT_T_WIN_RATIO.doubleValue())))
            .andExpect(jsonPath("$.[*].ctWinRatio").value(hasItem(DEFAULT_CT_WIN_RATIO.doubleValue())))
            .andExpect(jsonPath("$.[*].kdRatio").value(hasItem(DEFAULT_KD_RATIO.doubleValue())))
            .andExpect(jsonPath("$.[*].kdDiff").value(hasItem(DEFAULT_KD_DIFF.intValue())))
            .andExpect(jsonPath("$.[*].headshotRadio").value(hasItem(DEFAULT_HEADSHOT_RADIO.doubleValue())))
            .andExpect(jsonPath("$.[*].bestWeapon").value(hasItem(DEFAULT_BEST_WEAPON.toString())))
            .andExpect(jsonPath("$.[*].twitterUrl").value(hasItem(DEFAULT_TWITTER_URL.toString())))
            .andExpect(jsonPath("$.[*].twitchUrl").value(hasItem(DEFAULT_TWITCH_URL.toString())))
            .andExpect(jsonPath("$.[*].hardwareSpecs").value(hasItem(DEFAULT_HARDWARE_SPECS.toString())))
            .andExpect(jsonPath("$.[*].totalRounds").value(hasItem(DEFAULT_TOTAL_ROUNDS.intValue())))
            .andExpect(jsonPath("$.[*].grenadeDmgRound").value(hasItem(DEFAULT_GRENADE_DMG_ROUND.doubleValue())))
            .andExpect(jsonPath("$.[*].killRoundRatio").value(hasItem(DEFAULT_KILL_ROUND_RATIO.doubleValue())))
            .andExpect(jsonPath("$.[*].deathRoundRatio").value(hasItem(DEFAULT_DEATH_ROUND_RATIO.doubleValue())));
    }

    @Test
    @Transactional
    public void getCSGOPlayer() throws Exception {
        // Initialize the database
        cSGOPlayerRepository.saveAndFlush(cSGOPlayer);

        // Get the cSGOPlayer
        restCSGOPlayerMockMvc.perform(get("/api/csgo-players/{id}", cSGOPlayer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cSGOPlayer.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.age").value(DEFAULT_AGE.intValue()))
            .andExpect(jsonPath("$.nick").value(DEFAULT_NICK.toString()))
            .andExpect(jsonPath("$.rating").value(DEFAULT_RATING.doubleValue()))
            .andExpect(jsonPath("$.usersRating").value(DEFAULT_USERS_RATING.doubleValue()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .andExpect(jsonPath("$.mapsPlayed").value(DEFAULT_MAPS_PLAYED.intValue()))
            .andExpect(jsonPath("$.winLossRatio").value(DEFAULT_WIN_LOSS_RATIO.doubleValue()))
            .andExpect(jsonPath("$.tWinRatio").value(DEFAULT_T_WIN_RATIO.doubleValue()))
            .andExpect(jsonPath("$.ctWinRatio").value(DEFAULT_CT_WIN_RATIO.doubleValue()))
            .andExpect(jsonPath("$.kdRatio").value(DEFAULT_KD_RATIO.doubleValue()))
            .andExpect(jsonPath("$.kdDiff").value(DEFAULT_KD_DIFF.intValue()))
            .andExpect(jsonPath("$.headshotRadio").value(DEFAULT_HEADSHOT_RADIO.doubleValue()))
            .andExpect(jsonPath("$.bestWeapon").value(DEFAULT_BEST_WEAPON.toString()))
            .andExpect(jsonPath("$.twitterUrl").value(DEFAULT_TWITTER_URL.toString()))
            .andExpect(jsonPath("$.twitchUrl").value(DEFAULT_TWITCH_URL.toString()))
            .andExpect(jsonPath("$.hardwareSpecs").value(DEFAULT_HARDWARE_SPECS.toString()))
            .andExpect(jsonPath("$.totalRounds").value(DEFAULT_TOTAL_ROUNDS.intValue()))
            .andExpect(jsonPath("$.grenadeDmgRound").value(DEFAULT_GRENADE_DMG_ROUND.doubleValue()))
            .andExpect(jsonPath("$.killRoundRatio").value(DEFAULT_KILL_ROUND_RATIO.doubleValue()))
            .andExpect(jsonPath("$.deathRoundRatio").value(DEFAULT_DEATH_ROUND_RATIO.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCSGOPlayer() throws Exception {
        // Get the cSGOPlayer
        restCSGOPlayerMockMvc.perform(get("/api/csgo-players/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCSGOPlayer() throws Exception {
        // Initialize the database
        cSGOPlayerRepository.saveAndFlush(cSGOPlayer);
        int databaseSizeBeforeUpdate = cSGOPlayerRepository.findAll().size();

        // Update the cSGOPlayer
        CSGOPlayer updatedCSGOPlayer = cSGOPlayerRepository.findOne(cSGOPlayer.getId());
        // Disconnect from session so that the updates on updatedCSGOPlayer are not directly saved in db
        em.detach(updatedCSGOPlayer);
        updatedCSGOPlayer
            .name(UPDATED_NAME)
            .age(UPDATED_AGE)
            .nick(UPDATED_NICK)
            .rating(UPDATED_RATING)
            .usersRating(UPDATED_USERS_RATING)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .mapsPlayed(UPDATED_MAPS_PLAYED)
            .winLossRatio(UPDATED_WIN_LOSS_RATIO)
            .tWinRatio(UPDATED_T_WIN_RATIO)
            .ctWinRatio(UPDATED_CT_WIN_RATIO)
            .kdRatio(UPDATED_KD_RATIO)
            .kdDiff(UPDATED_KD_DIFF)
            .headshotRadio(UPDATED_HEADSHOT_RADIO)
            .bestWeapon(UPDATED_BEST_WEAPON)
            .twitterUrl(UPDATED_TWITTER_URL)
            .twitchUrl(UPDATED_TWITCH_URL)
            .hardwareSpecs(UPDATED_HARDWARE_SPECS)
            .totalRounds(UPDATED_TOTAL_ROUNDS)
            .grenadeDmgRound(UPDATED_GRENADE_DMG_ROUND)
            .killRoundRatio(UPDATED_KILL_ROUND_RATIO)
            .deathRoundRatio(UPDATED_DEATH_ROUND_RATIO);

        restCSGOPlayerMockMvc.perform(put("/api/csgo-players")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCSGOPlayer)))
            .andExpect(status().isOk());

        // Validate the CSGOPlayer in the database
        List<CSGOPlayer> cSGOPlayerList = cSGOPlayerRepository.findAll();
        assertThat(cSGOPlayerList).hasSize(databaseSizeBeforeUpdate);
        CSGOPlayer testCSGOPlayer = cSGOPlayerList.get(cSGOPlayerList.size() - 1);
        assertThat(testCSGOPlayer.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCSGOPlayer.getAge()).isEqualTo(UPDATED_AGE);
        assertThat(testCSGOPlayer.getNick()).isEqualTo(UPDATED_NICK);
        assertThat(testCSGOPlayer.getRating()).isEqualTo(UPDATED_RATING);
        assertThat(testCSGOPlayer.getUsersRating()).isEqualTo(UPDATED_USERS_RATING);
        assertThat(testCSGOPlayer.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testCSGOPlayer.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testCSGOPlayer.getMapsPlayed()).isEqualTo(UPDATED_MAPS_PLAYED);
        assertThat(testCSGOPlayer.getWinLossRatio()).isEqualTo(UPDATED_WIN_LOSS_RATIO);
        assertThat(testCSGOPlayer.gettWinRatio()).isEqualTo(UPDATED_T_WIN_RATIO);
        assertThat(testCSGOPlayer.getCtWinRatio()).isEqualTo(UPDATED_CT_WIN_RATIO);
        assertThat(testCSGOPlayer.getKdRatio()).isEqualTo(UPDATED_KD_RATIO);
        assertThat(testCSGOPlayer.getKdDiff()).isEqualTo(UPDATED_KD_DIFF);
        assertThat(testCSGOPlayer.getHeadshotRadio()).isEqualTo(UPDATED_HEADSHOT_RADIO);
        assertThat(testCSGOPlayer.getBestWeapon()).isEqualTo(UPDATED_BEST_WEAPON);
        assertThat(testCSGOPlayer.getTwitterUrl()).isEqualTo(UPDATED_TWITTER_URL);
        assertThat(testCSGOPlayer.getTwitchUrl()).isEqualTo(UPDATED_TWITCH_URL);
        assertThat(testCSGOPlayer.getHardwareSpecs()).isEqualTo(UPDATED_HARDWARE_SPECS);
        assertThat(testCSGOPlayer.getTotalRounds()).isEqualTo(UPDATED_TOTAL_ROUNDS);
        assertThat(testCSGOPlayer.getGrenadeDmgRound()).isEqualTo(UPDATED_GRENADE_DMG_ROUND);
        assertThat(testCSGOPlayer.getKillRoundRatio()).isEqualTo(UPDATED_KILL_ROUND_RATIO);
        assertThat(testCSGOPlayer.getDeathRoundRatio()).isEqualTo(UPDATED_DEATH_ROUND_RATIO);
    }

    @Test
    @Transactional
    public void updateNonExistingCSGOPlayer() throws Exception {
        int databaseSizeBeforeUpdate = cSGOPlayerRepository.findAll().size();

        // Create the CSGOPlayer

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCSGOPlayerMockMvc.perform(put("/api/csgo-players")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOPlayer)))
            .andExpect(status().isCreated());

        // Validate the CSGOPlayer in the database
        List<CSGOPlayer> cSGOPlayerList = cSGOPlayerRepository.findAll();
        assertThat(cSGOPlayerList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCSGOPlayer() throws Exception {
        // Initialize the database
        cSGOPlayerRepository.saveAndFlush(cSGOPlayer);
        int databaseSizeBeforeDelete = cSGOPlayerRepository.findAll().size();

        // Get the cSGOPlayer
        restCSGOPlayerMockMvc.perform(delete("/api/csgo-players/{id}", cSGOPlayer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CSGOPlayer> cSGOPlayerList = cSGOPlayerRepository.findAll();
        assertThat(cSGOPlayerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CSGOPlayer.class);
        CSGOPlayer cSGOPlayer1 = new CSGOPlayer();
        cSGOPlayer1.setId(1L);
        CSGOPlayer cSGOPlayer2 = new CSGOPlayer();
        cSGOPlayer2.setId(cSGOPlayer1.getId());
        assertThat(cSGOPlayer1).isEqualTo(cSGOPlayer2);
        cSGOPlayer2.setId(2L);
        assertThat(cSGOPlayer1).isNotEqualTo(cSGOPlayer2);
        cSGOPlayer1.setId(null);
        assertThat(cSGOPlayer1).isNotEqualTo(cSGOPlayer2);
    }
}
