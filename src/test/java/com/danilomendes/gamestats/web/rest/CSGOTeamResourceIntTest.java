package com.danilomendes.gamestats.web.rest;

import com.danilomendes.gamestats.GamestatsApp;

import com.danilomendes.gamestats.domain.CSGOTeam;
import com.danilomendes.gamestats.repository.CSGOTeamRepository;
import com.danilomendes.gamestats.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.danilomendes.gamestats.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CSGOTeamResource REST controller.
 *
 * @see CSGOTeamResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GamestatsApp.class)
public class CSGOTeamResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Double DEFAULT_RATING = 1D;
    private static final Double UPDATED_RATING = 2D;

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_USERS_RATING = 1D;
    private static final Double UPDATED_USERS_RATING = 2D;

    private static final Long DEFAULT_MAPS_PLAYED = 1L;
    private static final Long UPDATED_MAPS_PLAYED = 2L;

    private static final byte[] DEFAULT_PLAYERS_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PLAYERS_PHOTO = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_PLAYERS_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PLAYERS_PHOTO_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_TEAM_LOGO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_TEAM_LOGO = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_TEAM_LOGO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_TEAM_LOGO_CONTENT_TYPE = "image/png";

    private static final Double DEFAULT_CT_WIN_RATIO = 1D;
    private static final Double UPDATED_CT_WIN_RATIO = 2D;

    private static final Double DEFAULT_T_WIN_RATIO = 1D;
    private static final Double UPDATED_T_WIN_RATIO = 2D;

    private static final Long DEFAULT_NUMBER_OF_MAPS_PLAYED = 1L;
    private static final Long UPDATED_NUMBER_OF_MAPS_PLAYED = 2L;

    private static final String DEFAULT_TWITTER_URL = "AAAAAAAAAA";
    private static final String UPDATED_TWITTER_URL = "BBBBBBBBBB";

    @Autowired
    private CSGOTeamRepository cSGOTeamRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCSGOTeamMockMvc;

    private CSGOTeam cSGOTeam;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CSGOTeamResource cSGOTeamResource = new CSGOTeamResource(cSGOTeamRepository);
        this.restCSGOTeamMockMvc = MockMvcBuilders.standaloneSetup(cSGOTeamResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CSGOTeam createEntity(EntityManager em) {
        CSGOTeam cSGOTeam = new CSGOTeam()
            .name(DEFAULT_NAME)
            .rating(DEFAULT_RATING)
            .creationDate(DEFAULT_CREATION_DATE)
            .usersRating(DEFAULT_USERS_RATING)
            .mapsPlayed(DEFAULT_MAPS_PLAYED)
            .playersPhoto(DEFAULT_PLAYERS_PHOTO)
            .playersPhotoContentType(DEFAULT_PLAYERS_PHOTO_CONTENT_TYPE)
            .teamLogo(DEFAULT_TEAM_LOGO)
            .teamLogoContentType(DEFAULT_TEAM_LOGO_CONTENT_TYPE)
            .ctWinRatio(DEFAULT_CT_WIN_RATIO)
            .tWinRatio(DEFAULT_T_WIN_RATIO)
            .numberOfMapsPlayed(DEFAULT_NUMBER_OF_MAPS_PLAYED)
            .twitterUrl(DEFAULT_TWITTER_URL);
        return cSGOTeam;
    }

    @Before
    public void initTest() {
        cSGOTeam = createEntity(em);
    }

    @Test
    @Transactional
    public void createCSGOTeam() throws Exception {
        int databaseSizeBeforeCreate = cSGOTeamRepository.findAll().size();

        // Create the CSGOTeam
        restCSGOTeamMockMvc.perform(post("/api/csgo-teams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOTeam)))
            .andExpect(status().isCreated());

        // Validate the CSGOTeam in the database
        List<CSGOTeam> cSGOTeamList = cSGOTeamRepository.findAll();
        assertThat(cSGOTeamList).hasSize(databaseSizeBeforeCreate + 1);
        CSGOTeam testCSGOTeam = cSGOTeamList.get(cSGOTeamList.size() - 1);
        assertThat(testCSGOTeam.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCSGOTeam.getRating()).isEqualTo(DEFAULT_RATING);
        assertThat(testCSGOTeam.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testCSGOTeam.getUsersRating()).isEqualTo(DEFAULT_USERS_RATING);
        assertThat(testCSGOTeam.getMapsPlayed()).isEqualTo(DEFAULT_MAPS_PLAYED);
        assertThat(testCSGOTeam.getPlayersPhoto()).isEqualTo(DEFAULT_PLAYERS_PHOTO);
        assertThat(testCSGOTeam.getPlayersPhotoContentType()).isEqualTo(DEFAULT_PLAYERS_PHOTO_CONTENT_TYPE);
        assertThat(testCSGOTeam.getTeamLogo()).isEqualTo(DEFAULT_TEAM_LOGO);
        assertThat(testCSGOTeam.getTeamLogoContentType()).isEqualTo(DEFAULT_TEAM_LOGO_CONTENT_TYPE);
        assertThat(testCSGOTeam.getCtWinRatio()).isEqualTo(DEFAULT_CT_WIN_RATIO);
        assertThat(testCSGOTeam.gettWinRatio()).isEqualTo(DEFAULT_T_WIN_RATIO);
        assertThat(testCSGOTeam.getNumberOfMapsPlayed()).isEqualTo(DEFAULT_NUMBER_OF_MAPS_PLAYED);
        assertThat(testCSGOTeam.getTwitterUrl()).isEqualTo(DEFAULT_TWITTER_URL);
    }

    @Test
    @Transactional
    public void createCSGOTeamWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cSGOTeamRepository.findAll().size();

        // Create the CSGOTeam with an existing ID
        cSGOTeam.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCSGOTeamMockMvc.perform(post("/api/csgo-teams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOTeam)))
            .andExpect(status().isBadRequest());

        // Validate the CSGOTeam in the database
        List<CSGOTeam> cSGOTeamList = cSGOTeamRepository.findAll();
        assertThat(cSGOTeamList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCSGOTeams() throws Exception {
        // Initialize the database
        cSGOTeamRepository.saveAndFlush(cSGOTeam);

        // Get all the cSGOTeamList
        restCSGOTeamMockMvc.perform(get("/api/csgo-teams?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cSGOTeam.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].rating").value(hasItem(DEFAULT_RATING.doubleValue())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].usersRating").value(hasItem(DEFAULT_USERS_RATING.doubleValue())))
            .andExpect(jsonPath("$.[*].mapsPlayed").value(hasItem(DEFAULT_MAPS_PLAYED.intValue())))
            .andExpect(jsonPath("$.[*].playersPhotoContentType").value(hasItem(DEFAULT_PLAYERS_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].playersPhoto").value(hasItem(Base64Utils.encodeToString(DEFAULT_PLAYERS_PHOTO))))
            .andExpect(jsonPath("$.[*].teamLogoContentType").value(hasItem(DEFAULT_TEAM_LOGO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].teamLogo").value(hasItem(Base64Utils.encodeToString(DEFAULT_TEAM_LOGO))))
            .andExpect(jsonPath("$.[*].ctWinRatio").value(hasItem(DEFAULT_CT_WIN_RATIO.doubleValue())))
            .andExpect(jsonPath("$.[*].tWinRatio").value(hasItem(DEFAULT_T_WIN_RATIO.doubleValue())))
            .andExpect(jsonPath("$.[*].numberOfMapsPlayed").value(hasItem(DEFAULT_NUMBER_OF_MAPS_PLAYED.intValue())))
            .andExpect(jsonPath("$.[*].twitterUrl").value(hasItem(DEFAULT_TWITTER_URL.toString())));
    }

    @Test
    @Transactional
    public void getCSGOTeam() throws Exception {
        // Initialize the database
        cSGOTeamRepository.saveAndFlush(cSGOTeam);

        // Get the cSGOTeam
        restCSGOTeamMockMvc.perform(get("/api/csgo-teams/{id}", cSGOTeam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cSGOTeam.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.rating").value(DEFAULT_RATING.doubleValue()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.usersRating").value(DEFAULT_USERS_RATING.doubleValue()))
            .andExpect(jsonPath("$.mapsPlayed").value(DEFAULT_MAPS_PLAYED.intValue()))
            .andExpect(jsonPath("$.playersPhotoContentType").value(DEFAULT_PLAYERS_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.playersPhoto").value(Base64Utils.encodeToString(DEFAULT_PLAYERS_PHOTO)))
            .andExpect(jsonPath("$.teamLogoContentType").value(DEFAULT_TEAM_LOGO_CONTENT_TYPE))
            .andExpect(jsonPath("$.teamLogo").value(Base64Utils.encodeToString(DEFAULT_TEAM_LOGO)))
            .andExpect(jsonPath("$.ctWinRatio").value(DEFAULT_CT_WIN_RATIO.doubleValue()))
            .andExpect(jsonPath("$.tWinRatio").value(DEFAULT_T_WIN_RATIO.doubleValue()))
            .andExpect(jsonPath("$.numberOfMapsPlayed").value(DEFAULT_NUMBER_OF_MAPS_PLAYED.intValue()))
            .andExpect(jsonPath("$.twitterUrl").value(DEFAULT_TWITTER_URL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCSGOTeam() throws Exception {
        // Get the cSGOTeam
        restCSGOTeamMockMvc.perform(get("/api/csgo-teams/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCSGOTeam() throws Exception {
        // Initialize the database
        cSGOTeamRepository.saveAndFlush(cSGOTeam);
        int databaseSizeBeforeUpdate = cSGOTeamRepository.findAll().size();

        // Update the cSGOTeam
        CSGOTeam updatedCSGOTeam = cSGOTeamRepository.findOne(cSGOTeam.getId());
        // Disconnect from session so that the updates on updatedCSGOTeam are not directly saved in db
        em.detach(updatedCSGOTeam);
        updatedCSGOTeam
            .name(UPDATED_NAME)
            .rating(UPDATED_RATING)
            .creationDate(UPDATED_CREATION_DATE)
            .usersRating(UPDATED_USERS_RATING)
            .mapsPlayed(UPDATED_MAPS_PLAYED)
            .playersPhoto(UPDATED_PLAYERS_PHOTO)
            .playersPhotoContentType(UPDATED_PLAYERS_PHOTO_CONTENT_TYPE)
            .teamLogo(UPDATED_TEAM_LOGO)
            .teamLogoContentType(UPDATED_TEAM_LOGO_CONTENT_TYPE)
            .ctWinRatio(UPDATED_CT_WIN_RATIO)
            .tWinRatio(UPDATED_T_WIN_RATIO)
            .numberOfMapsPlayed(UPDATED_NUMBER_OF_MAPS_PLAYED)
            .twitterUrl(UPDATED_TWITTER_URL);

        restCSGOTeamMockMvc.perform(put("/api/csgo-teams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCSGOTeam)))
            .andExpect(status().isOk());

        // Validate the CSGOTeam in the database
        List<CSGOTeam> cSGOTeamList = cSGOTeamRepository.findAll();
        assertThat(cSGOTeamList).hasSize(databaseSizeBeforeUpdate);
        CSGOTeam testCSGOTeam = cSGOTeamList.get(cSGOTeamList.size() - 1);
        assertThat(testCSGOTeam.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCSGOTeam.getRating()).isEqualTo(UPDATED_RATING);
        assertThat(testCSGOTeam.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testCSGOTeam.getUsersRating()).isEqualTo(UPDATED_USERS_RATING);
        assertThat(testCSGOTeam.getMapsPlayed()).isEqualTo(UPDATED_MAPS_PLAYED);
        assertThat(testCSGOTeam.getPlayersPhoto()).isEqualTo(UPDATED_PLAYERS_PHOTO);
        assertThat(testCSGOTeam.getPlayersPhotoContentType()).isEqualTo(UPDATED_PLAYERS_PHOTO_CONTENT_TYPE);
        assertThat(testCSGOTeam.getTeamLogo()).isEqualTo(UPDATED_TEAM_LOGO);
        assertThat(testCSGOTeam.getTeamLogoContentType()).isEqualTo(UPDATED_TEAM_LOGO_CONTENT_TYPE);
        assertThat(testCSGOTeam.getCtWinRatio()).isEqualTo(UPDATED_CT_WIN_RATIO);
        assertThat(testCSGOTeam.gettWinRatio()).isEqualTo(UPDATED_T_WIN_RATIO);
        assertThat(testCSGOTeam.getNumberOfMapsPlayed()).isEqualTo(UPDATED_NUMBER_OF_MAPS_PLAYED);
        assertThat(testCSGOTeam.getTwitterUrl()).isEqualTo(UPDATED_TWITTER_URL);
    }

    @Test
    @Transactional
    public void updateNonExistingCSGOTeam() throws Exception {
        int databaseSizeBeforeUpdate = cSGOTeamRepository.findAll().size();

        // Create the CSGOTeam

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCSGOTeamMockMvc.perform(put("/api/csgo-teams")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOTeam)))
            .andExpect(status().isCreated());

        // Validate the CSGOTeam in the database
        List<CSGOTeam> cSGOTeamList = cSGOTeamRepository.findAll();
        assertThat(cSGOTeamList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCSGOTeam() throws Exception {
        // Initialize the database
        cSGOTeamRepository.saveAndFlush(cSGOTeam);
        int databaseSizeBeforeDelete = cSGOTeamRepository.findAll().size();

        // Get the cSGOTeam
        restCSGOTeamMockMvc.perform(delete("/api/csgo-teams/{id}", cSGOTeam.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CSGOTeam> cSGOTeamList = cSGOTeamRepository.findAll();
        assertThat(cSGOTeamList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CSGOTeam.class);
        CSGOTeam cSGOTeam1 = new CSGOTeam();
        cSGOTeam1.setId(1L);
        CSGOTeam cSGOTeam2 = new CSGOTeam();
        cSGOTeam2.setId(cSGOTeam1.getId());
        assertThat(cSGOTeam1).isEqualTo(cSGOTeam2);
        cSGOTeam2.setId(2L);
        assertThat(cSGOTeam1).isNotEqualTo(cSGOTeam2);
        cSGOTeam1.setId(null);
        assertThat(cSGOTeam1).isNotEqualTo(cSGOTeam2);
    }
}
