package com.danilomendes.gamestats.web.rest;

import com.danilomendes.gamestats.GamestatsApp;

import com.danilomendes.gamestats.domain.CSGOPlayerUrl;
import com.danilomendes.gamestats.repository.CSGOPlayerUrlRepository;
import com.danilomendes.gamestats.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.danilomendes.gamestats.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CSGOPlayerUrlResource REST controller.
 *
 * @see CSGOPlayerUrlResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GamestatsApp.class)
public class CSGOPlayerUrlResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_STATS_URL = "AAAAAAAAAA";
    private static final String UPDATED_STATS_URL = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY_URL = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_URL = "BBBBBBBBBB";

    private static final String DEFAULT_PHOTO_URL = "AAAAAAAAAA";
    private static final String UPDATED_PHOTO_URL = "BBBBBBBBBB";

    private static final Long DEFAULT_PLAYER_ID = 1L;
    private static final Long UPDATED_PLAYER_ID = 2L;

    @Autowired
    private CSGOPlayerUrlRepository cSGOPlayerUrlRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCSGOPlayerUrlMockMvc;

    private CSGOPlayerUrl cSGOPlayerUrl;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CSGOPlayerUrlResource cSGOPlayerUrlResource = new CSGOPlayerUrlResource(cSGOPlayerUrlRepository);
        this.restCSGOPlayerUrlMockMvc = MockMvcBuilders.standaloneSetup(cSGOPlayerUrlResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CSGOPlayerUrl createEntity(EntityManager em) {
        CSGOPlayerUrl cSGOPlayerUrl = new CSGOPlayerUrl()
            .name(DEFAULT_NAME)
            .statsUrl(DEFAULT_STATS_URL)
            .countryUrl(DEFAULT_COUNTRY_URL)
            .photoUrl(DEFAULT_PHOTO_URL)
            .playerId(DEFAULT_PLAYER_ID);
        return cSGOPlayerUrl;
    }

    @Before
    public void initTest() {
        cSGOPlayerUrl = createEntity(em);
    }

    @Test
    @Transactional
    public void createCSGOPlayerUrl() throws Exception {
        int databaseSizeBeforeCreate = cSGOPlayerUrlRepository.findAll().size();

        // Create the CSGOPlayerUrl
        restCSGOPlayerUrlMockMvc.perform(post("/api/csgo-player-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOPlayerUrl)))
            .andExpect(status().isCreated());

        // Validate the CSGOPlayerUrl in the database
        List<CSGOPlayerUrl> cSGOPlayerUrlList = cSGOPlayerUrlRepository.findAll();
        assertThat(cSGOPlayerUrlList).hasSize(databaseSizeBeforeCreate + 1);
        CSGOPlayerUrl testCSGOPlayerUrl = cSGOPlayerUrlList.get(cSGOPlayerUrlList.size() - 1);
        assertThat(testCSGOPlayerUrl.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCSGOPlayerUrl.getStatsUrl()).isEqualTo(DEFAULT_STATS_URL);
        assertThat(testCSGOPlayerUrl.getCountryUrl()).isEqualTo(DEFAULT_COUNTRY_URL);
        assertThat(testCSGOPlayerUrl.getPhotoUrl()).isEqualTo(DEFAULT_PHOTO_URL);
        assertThat(testCSGOPlayerUrl.getPlayerId()).isEqualTo(DEFAULT_PLAYER_ID);
    }

    @Test
    @Transactional
    public void createCSGOPlayerUrlWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cSGOPlayerUrlRepository.findAll().size();

        // Create the CSGOPlayerUrl with an existing ID
        cSGOPlayerUrl.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCSGOPlayerUrlMockMvc.perform(post("/api/csgo-player-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOPlayerUrl)))
            .andExpect(status().isBadRequest());

        // Validate the CSGOPlayerUrl in the database
        List<CSGOPlayerUrl> cSGOPlayerUrlList = cSGOPlayerUrlRepository.findAll();
        assertThat(cSGOPlayerUrlList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCSGOPlayerUrls() throws Exception {
        // Initialize the database
        cSGOPlayerUrlRepository.saveAndFlush(cSGOPlayerUrl);

        // Get all the cSGOPlayerUrlList
        restCSGOPlayerUrlMockMvc.perform(get("/api/csgo-player-urls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cSGOPlayerUrl.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].statsUrl").value(hasItem(DEFAULT_STATS_URL.toString())))
            .andExpect(jsonPath("$.[*].countryUrl").value(hasItem(DEFAULT_COUNTRY_URL.toString())))
            .andExpect(jsonPath("$.[*].photoUrl").value(hasItem(DEFAULT_PHOTO_URL.toString())))
            .andExpect(jsonPath("$.[*].playerId").value(hasItem(DEFAULT_PLAYER_ID.intValue())));
    }

    @Test
    @Transactional
    public void getCSGOPlayerUrl() throws Exception {
        // Initialize the database
        cSGOPlayerUrlRepository.saveAndFlush(cSGOPlayerUrl);

        // Get the cSGOPlayerUrl
        restCSGOPlayerUrlMockMvc.perform(get("/api/csgo-player-urls/{id}", cSGOPlayerUrl.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cSGOPlayerUrl.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.statsUrl").value(DEFAULT_STATS_URL.toString()))
            .andExpect(jsonPath("$.countryUrl").value(DEFAULT_COUNTRY_URL.toString()))
            .andExpect(jsonPath("$.photoUrl").value(DEFAULT_PHOTO_URL.toString()))
            .andExpect(jsonPath("$.playerId").value(DEFAULT_PLAYER_ID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingCSGOPlayerUrl() throws Exception {
        // Get the cSGOPlayerUrl
        restCSGOPlayerUrlMockMvc.perform(get("/api/csgo-player-urls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCSGOPlayerUrl() throws Exception {
        // Initialize the database
        cSGOPlayerUrlRepository.saveAndFlush(cSGOPlayerUrl);
        int databaseSizeBeforeUpdate = cSGOPlayerUrlRepository.findAll().size();

        // Update the cSGOPlayerUrl
        CSGOPlayerUrl updatedCSGOPlayerUrl = cSGOPlayerUrlRepository.findOne(cSGOPlayerUrl.getId());
        // Disconnect from session so that the updates on updatedCSGOPlayerUrl are not directly saved in db
        em.detach(updatedCSGOPlayerUrl);
        updatedCSGOPlayerUrl
            .name(UPDATED_NAME)
            .statsUrl(UPDATED_STATS_URL)
            .countryUrl(UPDATED_COUNTRY_URL)
            .photoUrl(UPDATED_PHOTO_URL)
            .playerId(UPDATED_PLAYER_ID);

        restCSGOPlayerUrlMockMvc.perform(put("/api/csgo-player-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCSGOPlayerUrl)))
            .andExpect(status().isOk());

        // Validate the CSGOPlayerUrl in the database
        List<CSGOPlayerUrl> cSGOPlayerUrlList = cSGOPlayerUrlRepository.findAll();
        assertThat(cSGOPlayerUrlList).hasSize(databaseSizeBeforeUpdate);
        CSGOPlayerUrl testCSGOPlayerUrl = cSGOPlayerUrlList.get(cSGOPlayerUrlList.size() - 1);
        assertThat(testCSGOPlayerUrl.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCSGOPlayerUrl.getStatsUrl()).isEqualTo(UPDATED_STATS_URL);
        assertThat(testCSGOPlayerUrl.getCountryUrl()).isEqualTo(UPDATED_COUNTRY_URL);
        assertThat(testCSGOPlayerUrl.getPhotoUrl()).isEqualTo(UPDATED_PHOTO_URL);
        assertThat(testCSGOPlayerUrl.getPlayerId()).isEqualTo(UPDATED_PLAYER_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingCSGOPlayerUrl() throws Exception {
        int databaseSizeBeforeUpdate = cSGOPlayerUrlRepository.findAll().size();

        // Create the CSGOPlayerUrl

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCSGOPlayerUrlMockMvc.perform(put("/api/csgo-player-urls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cSGOPlayerUrl)))
            .andExpect(status().isCreated());

        // Validate the CSGOPlayerUrl in the database
        List<CSGOPlayerUrl> cSGOPlayerUrlList = cSGOPlayerUrlRepository.findAll();
        assertThat(cSGOPlayerUrlList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCSGOPlayerUrl() throws Exception {
        // Initialize the database
        cSGOPlayerUrlRepository.saveAndFlush(cSGOPlayerUrl);
        int databaseSizeBeforeDelete = cSGOPlayerUrlRepository.findAll().size();

        // Get the cSGOPlayerUrl
        restCSGOPlayerUrlMockMvc.perform(delete("/api/csgo-player-urls/{id}", cSGOPlayerUrl.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CSGOPlayerUrl> cSGOPlayerUrlList = cSGOPlayerUrlRepository.findAll();
        assertThat(cSGOPlayerUrlList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CSGOPlayerUrl.class);
        CSGOPlayerUrl cSGOPlayerUrl1 = new CSGOPlayerUrl();
        cSGOPlayerUrl1.setId(1L);
        CSGOPlayerUrl cSGOPlayerUrl2 = new CSGOPlayerUrl();
        cSGOPlayerUrl2.setId(cSGOPlayerUrl1.getId());
        assertThat(cSGOPlayerUrl1).isEqualTo(cSGOPlayerUrl2);
        cSGOPlayerUrl2.setId(2L);
        assertThat(cSGOPlayerUrl1).isNotEqualTo(cSGOPlayerUrl2);
        cSGOPlayerUrl1.setId(null);
        assertThat(cSGOPlayerUrl1).isNotEqualTo(cSGOPlayerUrl2);
    }
}
