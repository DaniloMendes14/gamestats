/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GamestatsTestModule } from '../../../test.module';
import { AchievementDialogComponent } from '../../../../../../main/webapp/app/entities/achievement/achievement-dialog.component';
import { AchievementService } from '../../../../../../main/webapp/app/entities/achievement/achievement.service';
import { Achievement } from '../../../../../../main/webapp/app/entities/achievement/achievement.model';
import { CSGOTeamService } from '../../../../../../main/webapp/app/entities/csgo-team';
import { CSGOPlayerService } from '../../../../../../main/webapp/app/entities/csgo-player';

describe('Component Tests', () => {

    describe('Achievement Management Dialog Component', () => {
        let comp: AchievementDialogComponent;
        let fixture: ComponentFixture<AchievementDialogComponent>;
        let service: AchievementService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [AchievementDialogComponent],
                providers: [
                    CSGOTeamService,
                    CSGOPlayerService,
                    AchievementService
                ]
            })
            .overrideTemplate(AchievementDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AchievementDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AchievementService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Achievement(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.achievement = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'achievementListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Achievement();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.achievement = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'achievementListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
