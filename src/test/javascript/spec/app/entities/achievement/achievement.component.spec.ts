/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';

import { GamestatsTestModule } from '../../../test.module';
import { AchievementComponent } from '../../../../../../main/webapp/app/entities/achievement/achievement.component';
import { AchievementService } from '../../../../../../main/webapp/app/entities/achievement/achievement.service';
import { Achievement } from '../../../../../../main/webapp/app/entities/achievement/achievement.model';

describe('Component Tests', () => {

    describe('Achievement Management Component', () => {
        let comp: AchievementComponent;
        let fixture: ComponentFixture<AchievementComponent>;
        let service: AchievementService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [AchievementComponent],
                providers: [
                    AchievementService
                ]
            })
            .overrideTemplate(AchievementComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AchievementComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AchievementService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new Achievement(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.achievements[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
