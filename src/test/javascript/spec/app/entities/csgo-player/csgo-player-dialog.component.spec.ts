/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOPlayerDialogComponent } from '../../../../../../main/webapp/app/entities/csgo-player/csgo-player-dialog.component';
import { CSGOPlayerService } from '../../../../../../main/webapp/app/entities/csgo-player/csgo-player.service';
import { CSGOPlayer } from '../../../../../../main/webapp/app/entities/csgo-player/csgo-player.model';
import { RoundKillsService } from '../../../../../../main/webapp/app/entities/round-kills';
import { MatchService } from '../../../../../../main/webapp/app/entities/match';
import { CountryService } from '../../../../../../main/webapp/app/entities/country';
import { CSGOTeamService } from '../../../../../../main/webapp/app/entities/csgo-team';

describe('Component Tests', () => {

    describe('CSGOPlayer Management Dialog Component', () => {
        let comp: CSGOPlayerDialogComponent;
        let fixture: ComponentFixture<CSGOPlayerDialogComponent>;
        let service: CSGOPlayerService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOPlayerDialogComponent],
                providers: [
                    RoundKillsService,
                    MatchService,
                    CountryService,
                    CSGOTeamService,
                    CSGOPlayerService
                ]
            })
            .overrideTemplate(CSGOPlayerDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOPlayerDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOPlayerService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CSGOPlayer(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.cSGOPlayer = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'cSGOPlayerListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CSGOPlayer();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.cSGOPlayer = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'cSGOPlayerListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
