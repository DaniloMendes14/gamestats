/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOPlayerComponent } from '../../../../../../main/webapp/app/entities/csgo-player/csgo-player.component';
import { CSGOPlayerService } from '../../../../../../main/webapp/app/entities/csgo-player/csgo-player.service';
import { CSGOPlayer } from '../../../../../../main/webapp/app/entities/csgo-player/csgo-player.model';

describe('Component Tests', () => {

    describe('CSGOPlayer Management Component', () => {
        let comp: CSGOPlayerComponent;
        let fixture: ComponentFixture<CSGOPlayerComponent>;
        let service: CSGOPlayerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOPlayerComponent],
                providers: [
                    CSGOPlayerService
                ]
            })
            .overrideTemplate(CSGOPlayerComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOPlayerComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOPlayerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new CSGOPlayer(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.cSGOPlayers[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
