/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOPlayerDetailComponent } from '../../../../../../main/webapp/app/entities/csgo-player/csgo-player-detail.component';
import { CSGOPlayerService } from '../../../../../../main/webapp/app/entities/csgo-player/csgo-player.service';
import { CSGOPlayer } from '../../../../../../main/webapp/app/entities/csgo-player/csgo-player.model';

describe('Component Tests', () => {

    describe('CSGOPlayer Management Detail Component', () => {
        let comp: CSGOPlayerDetailComponent;
        let fixture: ComponentFixture<CSGOPlayerDetailComponent>;
        let service: CSGOPlayerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOPlayerDetailComponent],
                providers: [
                    CSGOPlayerService
                ]
            })
            .overrideTemplate(CSGOPlayerDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOPlayerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOPlayerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new CSGOPlayer(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.cSGOPlayer).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
