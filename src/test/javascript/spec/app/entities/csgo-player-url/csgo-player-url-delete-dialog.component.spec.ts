/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOPlayerUrlDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url-delete-dialog.component';
import { CSGOPlayerUrlService } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url.service';

describe('Component Tests', () => {

    describe('CSGOPlayerUrl Management Delete Component', () => {
        let comp: CSGOPlayerUrlDeleteDialogComponent;
        let fixture: ComponentFixture<CSGOPlayerUrlDeleteDialogComponent>;
        let service: CSGOPlayerUrlService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOPlayerUrlDeleteDialogComponent],
                providers: [
                    CSGOPlayerUrlService
                ]
            })
            .overrideTemplate(CSGOPlayerUrlDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOPlayerUrlDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOPlayerUrlService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
