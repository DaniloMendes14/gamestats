/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOPlayerUrlComponent } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url.component';
import { CSGOPlayerUrlService } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url.service';
import { CSGOPlayerUrl } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url.model';

describe('Component Tests', () => {

    describe('CSGOPlayerUrl Management Component', () => {
        let comp: CSGOPlayerUrlComponent;
        let fixture: ComponentFixture<CSGOPlayerUrlComponent>;
        let service: CSGOPlayerUrlService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOPlayerUrlComponent],
                providers: [
                    CSGOPlayerUrlService
                ]
            })
            .overrideTemplate(CSGOPlayerUrlComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOPlayerUrlComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOPlayerUrlService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new CSGOPlayerUrl(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.cSGOPlayerUrls[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
