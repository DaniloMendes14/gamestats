/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOPlayerUrlDetailComponent } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url-detail.component';
import { CSGOPlayerUrlService } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url.service';
import { CSGOPlayerUrl } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url.model';

describe('Component Tests', () => {

    describe('CSGOPlayerUrl Management Detail Component', () => {
        let comp: CSGOPlayerUrlDetailComponent;
        let fixture: ComponentFixture<CSGOPlayerUrlDetailComponent>;
        let service: CSGOPlayerUrlService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOPlayerUrlDetailComponent],
                providers: [
                    CSGOPlayerUrlService
                ]
            })
            .overrideTemplate(CSGOPlayerUrlDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOPlayerUrlDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOPlayerUrlService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new CSGOPlayerUrl(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.cSGOPlayerUrl).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
