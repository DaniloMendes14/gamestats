/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOPlayerUrlDialogComponent } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url-dialog.component';
import { CSGOPlayerUrlService } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url.service';
import { CSGOPlayerUrl } from '../../../../../../main/webapp/app/entities/csgo-player-url/csgo-player-url.model';

describe('Component Tests', () => {

    describe('CSGOPlayerUrl Management Dialog Component', () => {
        let comp: CSGOPlayerUrlDialogComponent;
        let fixture: ComponentFixture<CSGOPlayerUrlDialogComponent>;
        let service: CSGOPlayerUrlService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOPlayerUrlDialogComponent],
                providers: [
                    CSGOPlayerUrlService
                ]
            })
            .overrideTemplate(CSGOPlayerUrlDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOPlayerUrlDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOPlayerUrlService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CSGOPlayerUrl(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.cSGOPlayerUrl = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'cSGOPlayerUrlListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CSGOPlayerUrl();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.cSGOPlayerUrl = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'cSGOPlayerUrlListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
