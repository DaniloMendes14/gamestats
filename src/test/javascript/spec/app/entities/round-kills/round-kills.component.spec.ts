/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';

import { GamestatsTestModule } from '../../../test.module';
import { RoundKillsComponent } from '../../../../../../main/webapp/app/entities/round-kills/round-kills.component';
import { RoundKillsService } from '../../../../../../main/webapp/app/entities/round-kills/round-kills.service';
import { RoundKills } from '../../../../../../main/webapp/app/entities/round-kills/round-kills.model';

describe('Component Tests', () => {

    describe('RoundKills Management Component', () => {
        let comp: RoundKillsComponent;
        let fixture: ComponentFixture<RoundKillsComponent>;
        let service: RoundKillsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [RoundKillsComponent],
                providers: [
                    RoundKillsService
                ]
            })
            .overrideTemplate(RoundKillsComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RoundKillsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoundKillsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new RoundKills(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.roundKills[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
