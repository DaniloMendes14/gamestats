/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { GamestatsTestModule } from '../../../test.module';
import { RoundKillsDetailComponent } from '../../../../../../main/webapp/app/entities/round-kills/round-kills-detail.component';
import { RoundKillsService } from '../../../../../../main/webapp/app/entities/round-kills/round-kills.service';
import { RoundKills } from '../../../../../../main/webapp/app/entities/round-kills/round-kills.model';

describe('Component Tests', () => {

    describe('RoundKills Management Detail Component', () => {
        let comp: RoundKillsDetailComponent;
        let fixture: ComponentFixture<RoundKillsDetailComponent>;
        let service: RoundKillsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [RoundKillsDetailComponent],
                providers: [
                    RoundKillsService
                ]
            })
            .overrideTemplate(RoundKillsDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RoundKillsDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoundKillsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new RoundKills(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.roundKills).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
