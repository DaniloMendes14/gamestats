/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GamestatsTestModule } from '../../../test.module';
import { RoundKillsDialogComponent } from '../../../../../../main/webapp/app/entities/round-kills/round-kills-dialog.component';
import { RoundKillsService } from '../../../../../../main/webapp/app/entities/round-kills/round-kills.service';
import { RoundKills } from '../../../../../../main/webapp/app/entities/round-kills/round-kills.model';

describe('Component Tests', () => {

    describe('RoundKills Management Dialog Component', () => {
        let comp: RoundKillsDialogComponent;
        let fixture: ComponentFixture<RoundKillsDialogComponent>;
        let service: RoundKillsService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [RoundKillsDialogComponent],
                providers: [
                    RoundKillsService
                ]
            })
            .overrideTemplate(RoundKillsDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RoundKillsDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RoundKillsService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new RoundKills(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.roundKills = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'roundKillsListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new RoundKills();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.roundKills = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'roundKillsListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
