/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOTeamUrlDetailComponent } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url-detail.component';
import { CSGOTeamUrlService } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url.service';
import { CSGOTeamUrl } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url.model';

describe('Component Tests', () => {

    describe('CSGOTeamUrl Management Detail Component', () => {
        let comp: CSGOTeamUrlDetailComponent;
        let fixture: ComponentFixture<CSGOTeamUrlDetailComponent>;
        let service: CSGOTeamUrlService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOTeamUrlDetailComponent],
                providers: [
                    CSGOTeamUrlService
                ]
            })
            .overrideTemplate(CSGOTeamUrlDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOTeamUrlDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOTeamUrlService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new CSGOTeamUrl(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.cSGOTeamUrl).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
