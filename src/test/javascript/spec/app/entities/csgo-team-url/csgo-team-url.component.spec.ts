/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOTeamUrlComponent } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url.component';
import { CSGOTeamUrlService } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url.service';
import { CSGOTeamUrl } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url.model';

describe('Component Tests', () => {

    describe('CSGOTeamUrl Management Component', () => {
        let comp: CSGOTeamUrlComponent;
        let fixture: ComponentFixture<CSGOTeamUrlComponent>;
        let service: CSGOTeamUrlService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOTeamUrlComponent],
                providers: [
                    CSGOTeamUrlService
                ]
            })
            .overrideTemplate(CSGOTeamUrlComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOTeamUrlComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOTeamUrlService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new CSGOTeamUrl(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.cSGOTeamUrls[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
