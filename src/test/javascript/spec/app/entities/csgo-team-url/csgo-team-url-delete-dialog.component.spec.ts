/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOTeamUrlDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url-delete-dialog.component';
import { CSGOTeamUrlService } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url.service';

describe('Component Tests', () => {

    describe('CSGOTeamUrl Management Delete Component', () => {
        let comp: CSGOTeamUrlDeleteDialogComponent;
        let fixture: ComponentFixture<CSGOTeamUrlDeleteDialogComponent>;
        let service: CSGOTeamUrlService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOTeamUrlDeleteDialogComponent],
                providers: [
                    CSGOTeamUrlService
                ]
            })
            .overrideTemplate(CSGOTeamUrlDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOTeamUrlDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOTeamUrlService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
