/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOTeamUrlDialogComponent } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url-dialog.component';
import { CSGOTeamUrlService } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url.service';
import { CSGOTeamUrl } from '../../../../../../main/webapp/app/entities/csgo-team-url/csgo-team-url.model';

describe('Component Tests', () => {

    describe('CSGOTeamUrl Management Dialog Component', () => {
        let comp: CSGOTeamUrlDialogComponent;
        let fixture: ComponentFixture<CSGOTeamUrlDialogComponent>;
        let service: CSGOTeamUrlService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOTeamUrlDialogComponent],
                providers: [
                    CSGOTeamUrlService
                ]
            })
            .overrideTemplate(CSGOTeamUrlDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOTeamUrlDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOTeamUrlService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CSGOTeamUrl(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.cSGOTeamUrl = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'cSGOTeamUrlListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CSGOTeamUrl();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.cSGOTeamUrl = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'cSGOTeamUrlListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
