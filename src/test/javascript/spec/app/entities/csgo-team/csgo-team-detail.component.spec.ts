/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOTeamDetailComponent } from '../../../../../../main/webapp/app/entities/csgo-team/csgo-team-detail.component';
import { CSGOTeamService } from '../../../../../../main/webapp/app/entities/csgo-team/csgo-team.service';
import { CSGOTeam } from '../../../../../../main/webapp/app/entities/csgo-team/csgo-team.model';

describe('Component Tests', () => {

    describe('CSGOTeam Management Detail Component', () => {
        let comp: CSGOTeamDetailComponent;
        let fixture: ComponentFixture<CSGOTeamDetailComponent>;
        let service: CSGOTeamService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOTeamDetailComponent],
                providers: [
                    CSGOTeamService
                ]
            })
            .overrideTemplate(CSGOTeamDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOTeamDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOTeamService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new CSGOTeam(123)));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.cSGOTeam).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
