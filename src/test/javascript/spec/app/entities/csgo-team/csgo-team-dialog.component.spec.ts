/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOTeamDialogComponent } from '../../../../../../main/webapp/app/entities/csgo-team/csgo-team-dialog.component';
import { CSGOTeamService } from '../../../../../../main/webapp/app/entities/csgo-team/csgo-team.service';
import { CSGOTeam } from '../../../../../../main/webapp/app/entities/csgo-team/csgo-team.model';
import { CountryService } from '../../../../../../main/webapp/app/entities/country';
import { MatchService } from '../../../../../../main/webapp/app/entities/match';
import { RoundKillsService } from '../../../../../../main/webapp/app/entities/round-kills';

describe('Component Tests', () => {

    describe('CSGOTeam Management Dialog Component', () => {
        let comp: CSGOTeamDialogComponent;
        let fixture: ComponentFixture<CSGOTeamDialogComponent>;
        let service: CSGOTeamService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOTeamDialogComponent],
                providers: [
                    CountryService,
                    MatchService,
                    RoundKillsService,
                    CSGOTeamService
                ]
            })
            .overrideTemplate(CSGOTeamDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOTeamDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOTeamService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CSGOTeam(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.cSGOTeam = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'cSGOTeamListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new CSGOTeam();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.cSGOTeam = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'cSGOTeamListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
