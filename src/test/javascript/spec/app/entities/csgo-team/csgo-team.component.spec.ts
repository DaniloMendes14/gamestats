/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';

import { GamestatsTestModule } from '../../../test.module';
import { CSGOTeamComponent } from '../../../../../../main/webapp/app/entities/csgo-team/csgo-team.component';
import { CSGOTeamService } from '../../../../../../main/webapp/app/entities/csgo-team/csgo-team.service';
import { CSGOTeam } from '../../../../../../main/webapp/app/entities/csgo-team/csgo-team.model';

describe('Component Tests', () => {

    describe('CSGOTeam Management Component', () => {
        let comp: CSGOTeamComponent;
        let fixture: ComponentFixture<CSGOTeamComponent>;
        let service: CSGOTeamService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GamestatsTestModule],
                declarations: [CSGOTeamComponent],
                providers: [
                    CSGOTeamService
                ]
            })
            .overrideTemplate(CSGOTeamComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CSGOTeamComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CSGOTeamService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new CSGOTeam(123)],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.cSGOTeams[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
