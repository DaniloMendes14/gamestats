import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Map } from './map.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class MapService {

    private resourceUrl =  SERVER_API_URL + 'api/maps';

    constructor(private http: Http) { }

    create(map: Map): Observable<Map> {
        const copy = this.convert(map);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(map: Map): Observable<Map> {
        const copy = this.convert(map);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Map> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Map.
     */
    private convertItemFromServer(json: any): Map {
        const entity: Map = Object.assign(new Map(), json);
        return entity;
    }

    /**
     * Convert a Map to a JSON which can be sent to the server.
     */
    private convert(map: Map): Map {
        const copy: Map = Object.assign({}, map);
        return copy;
    }
}
