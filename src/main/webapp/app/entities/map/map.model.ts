import { BaseEntity } from './../../shared';

export class Map implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public usersRating?: number,
        public ctWinRatio?: number,
        public tWinRatio?: number,
        public numberPlayed?: number,
    ) {
    }
}
