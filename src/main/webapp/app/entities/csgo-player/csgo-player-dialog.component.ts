import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { CSGOPlayer } from './csgo-player.model';
import { CSGOPlayerPopupService } from './csgo-player-popup.service';
import { CSGOPlayerService } from './csgo-player.service';
import { RoundKills, RoundKillsService } from '../round-kills';
import { Match, MatchService } from '../match';
import { Country, CountryService } from '../country';
import { CSGOTeam, CSGOTeamService } from '../csgo-team';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-csgo-player-dialog',
    templateUrl: './csgo-player-dialog.component.html'
})
export class CSGOPlayerDialogComponent implements OnInit {

    cSGOPlayer: CSGOPlayer;
    isSaving: boolean;

    roundkills: RoundKills[];

    lastmatches: Match[];

    countries: Country[];

    csgoteams: CSGOTeam[];

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private cSGOPlayerService: CSGOPlayerService,
        private roundKillsService: RoundKillsService,
        private matchService: MatchService,
        private countryService: CountryService,
        private cSGOTeamService: CSGOTeamService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.roundKillsService
            .query({filter: 'csgoplayer-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.cSGOPlayer.roundKills || !this.cSGOPlayer.roundKills.id) {
                    this.roundkills = res.json;
                } else {
                    this.roundKillsService
                        .find(this.cSGOPlayer.roundKills.id)
                        .subscribe((subRes: RoundKills) => {
                            this.roundkills = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.matchService
            .query({filter: 'csgoplayer-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.cSGOPlayer.lastMatch || !this.cSGOPlayer.lastMatch.id) {
                    this.lastmatches = res.json;
                } else {
                    this.matchService
                        .find(this.cSGOPlayer.lastMatch.id)
                        .subscribe((subRes: Match) => {
                            this.lastmatches = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.countryService
            .query({filter: 'csgoplayer-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.cSGOPlayer.country || !this.cSGOPlayer.country.id) {
                    this.countries = res.json;
                } else {
                    this.countryService
                        .find(this.cSGOPlayer.country.id)
                        .subscribe((subRes: Country) => {
                            this.countries = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.cSGOTeamService.query()
            .subscribe((res: ResponseWrapper) => { this.csgoteams = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.cSGOPlayer.id !== undefined) {
            this.subscribeToSaveResponse(
                this.cSGOPlayerService.update(this.cSGOPlayer));
        } else {
            this.subscribeToSaveResponse(
                this.cSGOPlayerService.create(this.cSGOPlayer));
        }
    }

    private subscribeToSaveResponse(result: Observable<CSGOPlayer>) {
        result.subscribe((res: CSGOPlayer) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CSGOPlayer) {
        this.eventManager.broadcast({ name: 'cSGOPlayerListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRoundKillsById(index: number, item: RoundKills) {
        return item.id;
    }

    trackMatchById(index: number, item: Match) {
        return item.id;
    }

    trackCountryById(index: number, item: Country) {
        return item.id;
    }

    trackCSGOTeamById(index: number, item: CSGOTeam) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-csgo-player-popup',
    template: ''
})
export class CSGOPlayerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cSGOPlayerPopupService: CSGOPlayerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.cSGOPlayerPopupService
                    .open(CSGOPlayerDialogComponent as Component, params['id']);
            } else {
                this.cSGOPlayerPopupService
                    .open(CSGOPlayerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
