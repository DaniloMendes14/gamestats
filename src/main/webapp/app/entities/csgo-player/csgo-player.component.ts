import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { CSGOPlayer } from './csgo-player.model';
import { CSGOPlayerService } from './csgo-player.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-csgo-player',
    templateUrl: './csgo-player.component.html'
})
export class CSGOPlayerComponent implements OnInit, OnDestroy {
cSGOPlayers: CSGOPlayer[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private cSGOPlayerService: CSGOPlayerService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.cSGOPlayerService.query().subscribe(
            (res: ResponseWrapper) => {
                this.cSGOPlayers = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCSGOPlayers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CSGOPlayer) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    registerChangeInCSGOPlayers() {
        this.eventSubscriber = this.eventManager.subscribe('cSGOPlayerListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
