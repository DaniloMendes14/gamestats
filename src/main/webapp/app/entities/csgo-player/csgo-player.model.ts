import { BaseEntity } from './../../shared';

export class CSGOPlayer implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public age?: number,
        public nick?: string,
        public rating?: number,
        public usersRating?: number,
        public imageContentType?: string,
        public image?: any,
        public mapsPlayed?: number,
        public winLossRatio?: number,
        public tWinRatio?: number,
        public ctWinRatio?: number,
        public kdRatio?: number,
        public kdDiff?: number,
        public headshotRadio?: number,
        public bestWeapon?: string,
        public twitterUrl?: string,
        public twitchUrl?: string,
        public hardwareSpecs?: string,
        public totalRounds?: number,
        public grenadeDmgRound?: number,
        public killRoundRatio?: number,
        public deathRoundRatio?: number,
        public totalKill?: number,
        public totalDeath?:number,
        public roundKills?: BaseEntity,
        public lastMatch?: BaseEntity,
        public country?: BaseEntity,
        public team?: BaseEntity,
    ) {
    }
}
