import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CSGOPlayer } from './csgo-player.model';
import { CSGOPlayerPopupService } from './csgo-player-popup.service';
import { CSGOPlayerService } from './csgo-player.service';

@Component({
    selector: 'jhi-csgo-player-delete-dialog',
    templateUrl: './csgo-player-delete-dialog.component.html'
})
export class CSGOPlayerDeleteDialogComponent {

    cSGOPlayer: CSGOPlayer;

    constructor(
        private cSGOPlayerService: CSGOPlayerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.cSGOPlayerService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'cSGOPlayerListModification',
                content: 'Deleted an cSGOPlayer'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-csgo-player-delete-popup',
    template: ''
})
export class CSGOPlayerDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cSGOPlayerPopupService: CSGOPlayerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.cSGOPlayerPopupService
                .open(CSGOPlayerDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
