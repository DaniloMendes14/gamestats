import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GamestatsSharedModule } from '../../shared';
import {
    CSGOPlayerService,
    CSGOPlayerPopupService,
    CSGOPlayerComponent,
    CSGOPlayerDetailComponent,
    CSGOPlayerDialogComponent,
    CSGOPlayerPopupComponent,
    CSGOPlayerDeletePopupComponent,
    CSGOPlayerDeleteDialogComponent,
    cSGOPlayerRoute,
    cSGOPlayerPopupRoute,
} from './';

const ENTITY_STATES = [
    ...cSGOPlayerRoute,
    ...cSGOPlayerPopupRoute,
];

@NgModule({
    imports: [
        GamestatsSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CSGOPlayerComponent,
        CSGOPlayerDetailComponent,
        CSGOPlayerDialogComponent,
        CSGOPlayerDeleteDialogComponent,
        CSGOPlayerPopupComponent,
        CSGOPlayerDeletePopupComponent,
    ],
    entryComponents: [
        CSGOPlayerComponent,
        CSGOPlayerDialogComponent,
        CSGOPlayerPopupComponent,
        CSGOPlayerDeleteDialogComponent,
        CSGOPlayerDeletePopupComponent,
    ],
    providers: [
        CSGOPlayerService,
        CSGOPlayerPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GamestatsCSGOPlayerModule {}
