import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { CSGOPlayer } from './csgo-player.model';
import { CSGOPlayerService } from './csgo-player.service';

@Component({
    selector: 'jhi-csgo-player-detail',
    templateUrl: './csgo-player-detail.component.html'
})
export class CSGOPlayerDetailComponent implements OnInit, OnDestroy {

    cSGOPlayer: CSGOPlayer;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private cSGOPlayerService: CSGOPlayerService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCSGOPlayers();
    }

    load(id) {
        this.cSGOPlayerService.find(id).subscribe((cSGOPlayer) => {
            this.cSGOPlayer = cSGOPlayer;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCSGOPlayers() {
        this.eventSubscriber = this.eventManager.subscribe(
            'cSGOPlayerListModification',
            (response) => this.load(this.cSGOPlayer.id)
        );
    }
}
