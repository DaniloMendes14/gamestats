import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CSGOPlayerComponent } from './csgo-player.component';
import { CSGOPlayerDetailComponent } from './csgo-player-detail.component';
import { CSGOPlayerPopupComponent } from './csgo-player-dialog.component';
import { CSGOPlayerDeletePopupComponent } from './csgo-player-delete-dialog.component';

export const cSGOPlayerRoute: Routes = [
    {
        path: 'csgo-player',
        component: CSGOPlayerComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOPlayer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'csgo-player/:id',
        component: CSGOPlayerDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOPlayer.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cSGOPlayerPopupRoute: Routes = [
    {
        path: 'csgo-player-new',
        component: CSGOPlayerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOPlayer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'csgo-player/:id/edit',
        component: CSGOPlayerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOPlayer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'csgo-player/:id/delete',
        component: CSGOPlayerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOPlayer.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
