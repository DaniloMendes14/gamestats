import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { CSGOTeam } from './csgo-team.model';
import { CSGOTeamService } from './csgo-team.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-csgo-team',
    templateUrl: './csgo-team.component.html'
})
export class CSGOTeamComponent implements OnInit, OnDestroy {
cSGOTeams: CSGOTeam[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private cSGOTeamService: CSGOTeamService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.cSGOTeamService.query().subscribe(
            (res: ResponseWrapper) => {
                this.cSGOTeams = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCSGOTeams();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CSGOTeam) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    registerChangeInCSGOTeams() {
        this.eventSubscriber = this.eventManager.subscribe('cSGOTeamListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
