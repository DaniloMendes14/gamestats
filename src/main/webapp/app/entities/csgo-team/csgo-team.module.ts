import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GamestatsSharedModule } from '../../shared';
import {
    CSGOTeamService,
    CSGOTeamPopupService,
    CSGOTeamComponent,
    CSGOTeamDetailComponent,
    CSGOTeamDialogComponent,
    CSGOTeamPopupComponent,
    CSGOTeamDeletePopupComponent,
    CSGOTeamDeleteDialogComponent,
    cSGOTeamRoute,
    cSGOTeamPopupRoute,
} from './';

const ENTITY_STATES = [
    ...cSGOTeamRoute,
    ...cSGOTeamPopupRoute,
];

@NgModule({
    imports: [
        GamestatsSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CSGOTeamComponent,
        CSGOTeamDetailComponent,
        CSGOTeamDialogComponent,
        CSGOTeamDeleteDialogComponent,
        CSGOTeamPopupComponent,
        CSGOTeamDeletePopupComponent,
    ],
    entryComponents: [
        CSGOTeamComponent,
        CSGOTeamDialogComponent,
        CSGOTeamPopupComponent,
        CSGOTeamDeleteDialogComponent,
        CSGOTeamDeletePopupComponent,
    ],
    providers: [
        CSGOTeamService,
        CSGOTeamPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GamestatsCSGOTeamModule {}
