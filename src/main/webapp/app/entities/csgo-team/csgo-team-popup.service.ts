import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CSGOTeam } from './csgo-team.model';
import { CSGOTeamService } from './csgo-team.service';

@Injectable()
export class CSGOTeamPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private cSGOTeamService: CSGOTeamService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.cSGOTeamService.find(id).subscribe((cSGOTeam) => {
                    if (cSGOTeam.creationDate) {
                        cSGOTeam.creationDate = {
                            year: cSGOTeam.creationDate.getFullYear(),
                            month: cSGOTeam.creationDate.getMonth() + 1,
                            day: cSGOTeam.creationDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.cSGOTeamModalRef(component, cSGOTeam);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.cSGOTeamModalRef(component, new CSGOTeam());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    cSGOTeamModalRef(component: Component, cSGOTeam: CSGOTeam): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.cSGOTeam = cSGOTeam;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
