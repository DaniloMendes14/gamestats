import { BaseEntity } from './../../shared';

export class CSGOTeam implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public rating?: number,
        public creationDate?: any,
        public usersRating?: number,
        public mapsPlayed?: number,
        public playersPhotoContentType?: string,
        public playersPhoto?: any,
        public teamLogoContentType?: string,
        public teamLogo?: any,
        public ctWinRatio?: number,
        public tWinRatio?: number,
        public numberOfMapsPlayed?: number,
        public twitterUrl?: string,
        public country?: BaseEntity,
        public lastMatch?: BaseEntity,
        public roundKills?: BaseEntity,
    ) {
    }
}
