import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { CSGOTeam } from './csgo-team.model';
import { CSGOTeamService } from './csgo-team.service';

@Component({
    selector: 'jhi-csgo-team-detail',
    templateUrl: './csgo-team-detail.component.html'
})
export class CSGOTeamDetailComponent implements OnInit, OnDestroy {

    cSGOTeam: CSGOTeam;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private cSGOTeamService: CSGOTeamService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCSGOTeams();
    }

    load(id) {
        this.cSGOTeamService.find(id).subscribe((cSGOTeam) => {
            this.cSGOTeam = cSGOTeam;
        });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCSGOTeams() {
        this.eventSubscriber = this.eventManager.subscribe(
            'cSGOTeamListModification',
            (response) => this.load(this.cSGOTeam.id)
        );
    }
}
