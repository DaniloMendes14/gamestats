import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CSGOTeam } from './csgo-team.model';
import { CSGOTeamPopupService } from './csgo-team-popup.service';
import { CSGOTeamService } from './csgo-team.service';

@Component({
    selector: 'jhi-csgo-team-delete-dialog',
    templateUrl: './csgo-team-delete-dialog.component.html'
})
export class CSGOTeamDeleteDialogComponent {

    cSGOTeam: CSGOTeam;

    constructor(
        private cSGOTeamService: CSGOTeamService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.cSGOTeamService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'cSGOTeamListModification',
                content: 'Deleted an cSGOTeam'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-csgo-team-delete-popup',
    template: ''
})
export class CSGOTeamDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cSGOTeamPopupService: CSGOTeamPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.cSGOTeamPopupService
                .open(CSGOTeamDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
