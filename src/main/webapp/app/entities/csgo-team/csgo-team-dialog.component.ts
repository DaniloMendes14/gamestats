import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { CSGOTeam } from './csgo-team.model';
import { CSGOTeamPopupService } from './csgo-team-popup.service';
import { CSGOTeamService } from './csgo-team.service';
import { Country, CountryService } from '../country';
import { Match, MatchService } from '../match';
import { RoundKills, RoundKillsService } from '../round-kills';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-csgo-team-dialog',
    templateUrl: './csgo-team-dialog.component.html'
})
export class CSGOTeamDialogComponent implements OnInit {

    cSGOTeam: CSGOTeam;
    isSaving: boolean;

    countries: Country[];

    lastmatches: Match[];

    roundkills: RoundKills[];
    creationDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private jhiAlertService: JhiAlertService,
        private cSGOTeamService: CSGOTeamService,
        private countryService: CountryService,
        private matchService: MatchService,
        private roundKillsService: RoundKillsService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.countryService
            .query({filter: 'csgoteam-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.cSGOTeam.country || !this.cSGOTeam.country.id) {
                    this.countries = res.json;
                } else {
                    this.countryService
                        .find(this.cSGOTeam.country.id)
                        .subscribe((subRes: Country) => {
                            this.countries = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.matchService
            .query({filter: 'csgoteam-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.cSGOTeam.lastMatch || !this.cSGOTeam.lastMatch.id) {
                    this.lastmatches = res.json;
                } else {
                    this.matchService
                        .find(this.cSGOTeam.lastMatch.id)
                        .subscribe((subRes: Match) => {
                            this.lastmatches = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.roundKillsService
            .query({filter: 'csgoteam-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.cSGOTeam.roundKills || !this.cSGOTeam.roundKills.id) {
                    this.roundkills = res.json;
                } else {
                    this.roundKillsService
                        .find(this.cSGOTeam.roundKills.id)
                        .subscribe((subRes: RoundKills) => {
                            this.roundkills = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.cSGOTeam.id !== undefined) {
            this.subscribeToSaveResponse(
                this.cSGOTeamService.update(this.cSGOTeam));
        } else {
            this.subscribeToSaveResponse(
                this.cSGOTeamService.create(this.cSGOTeam));
        }
    }

    private subscribeToSaveResponse(result: Observable<CSGOTeam>) {
        result.subscribe((res: CSGOTeam) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CSGOTeam) {
        this.eventManager.broadcast({ name: 'cSGOTeamListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCountryById(index: number, item: Country) {
        return item.id;
    }

    trackMatchById(index: number, item: Match) {
        return item.id;
    }

    trackRoundKillsById(index: number, item: RoundKills) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-csgo-team-popup',
    template: ''
})
export class CSGOTeamPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cSGOTeamPopupService: CSGOTeamPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.cSGOTeamPopupService
                    .open(CSGOTeamDialogComponent as Component, params['id']);
            } else {
                this.cSGOTeamPopupService
                    .open(CSGOTeamDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
