import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CSGOTeamComponent } from './csgo-team.component';
import { CSGOTeamDetailComponent } from './csgo-team-detail.component';
import { CSGOTeamPopupComponent } from './csgo-team-dialog.component';
import { CSGOTeamDeletePopupComponent } from './csgo-team-delete-dialog.component';

export const cSGOTeamRoute: Routes = [
    {
        path: 'csgo-team',
        component: CSGOTeamComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOTeam.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'csgo-team/:id',
        component: CSGOTeamDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOTeam.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cSGOTeamPopupRoute: Routes = [
    {
        path: 'csgo-team-new',
        component: CSGOTeamPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOTeam.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'csgo-team/:id/edit',
        component: CSGOTeamPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOTeam.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'csgo-team/:id/delete',
        component: CSGOTeamDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOTeam.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
