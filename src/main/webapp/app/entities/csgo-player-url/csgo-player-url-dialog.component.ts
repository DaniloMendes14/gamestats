import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CSGOPlayerUrl } from './csgo-player-url.model';
import { CSGOPlayerUrlPopupService } from './csgo-player-url-popup.service';
import { CSGOPlayerUrlService } from './csgo-player-url.service';

@Component({
    selector: 'jhi-csgo-player-url-dialog',
    templateUrl: './csgo-player-url-dialog.component.html'
})
export class CSGOPlayerUrlDialogComponent implements OnInit {

    cSGOPlayerUrl: CSGOPlayerUrl;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private cSGOPlayerUrlService: CSGOPlayerUrlService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.cSGOPlayerUrl.id !== undefined) {
            this.subscribeToSaveResponse(
                this.cSGOPlayerUrlService.update(this.cSGOPlayerUrl));
        } else {
            this.subscribeToSaveResponse(
                this.cSGOPlayerUrlService.create(this.cSGOPlayerUrl));
        }
    }

    private subscribeToSaveResponse(result: Observable<CSGOPlayerUrl>) {
        result.subscribe((res: CSGOPlayerUrl) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CSGOPlayerUrl) {
        this.eventManager.broadcast({ name: 'cSGOPlayerUrlListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-csgo-player-url-popup',
    template: ''
})
export class CSGOPlayerUrlPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cSGOPlayerUrlPopupService: CSGOPlayerUrlPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.cSGOPlayerUrlPopupService
                    .open(CSGOPlayerUrlDialogComponent as Component, params['id']);
            } else {
                this.cSGOPlayerUrlPopupService
                    .open(CSGOPlayerUrlDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
