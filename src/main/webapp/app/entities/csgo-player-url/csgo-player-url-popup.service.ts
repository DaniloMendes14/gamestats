import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CSGOPlayerUrl } from './csgo-player-url.model';
import { CSGOPlayerUrlService } from './csgo-player-url.service';

@Injectable()
export class CSGOPlayerUrlPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private cSGOPlayerUrlService: CSGOPlayerUrlService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.cSGOPlayerUrlService.find(id).subscribe((cSGOPlayerUrl) => {
                    this.ngbModalRef = this.cSGOPlayerUrlModalRef(component, cSGOPlayerUrl);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.cSGOPlayerUrlModalRef(component, new CSGOPlayerUrl());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    cSGOPlayerUrlModalRef(component: Component, cSGOPlayerUrl: CSGOPlayerUrl): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.cSGOPlayerUrl = cSGOPlayerUrl;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
