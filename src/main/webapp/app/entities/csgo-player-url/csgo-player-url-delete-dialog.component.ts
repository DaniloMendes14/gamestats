import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CSGOPlayerUrl } from './csgo-player-url.model';
import { CSGOPlayerUrlPopupService } from './csgo-player-url-popup.service';
import { CSGOPlayerUrlService } from './csgo-player-url.service';

@Component({
    selector: 'jhi-csgo-player-url-delete-dialog',
    templateUrl: './csgo-player-url-delete-dialog.component.html'
})
export class CSGOPlayerUrlDeleteDialogComponent {

    cSGOPlayerUrl: CSGOPlayerUrl;

    constructor(
        private cSGOPlayerUrlService: CSGOPlayerUrlService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.cSGOPlayerUrlService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'cSGOPlayerUrlListModification',
                content: 'Deleted an cSGOPlayerUrl'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-csgo-player-url-delete-popup',
    template: ''
})
export class CSGOPlayerUrlDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cSGOPlayerUrlPopupService: CSGOPlayerUrlPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.cSGOPlayerUrlPopupService
                .open(CSGOPlayerUrlDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
