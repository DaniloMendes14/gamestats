import { BaseEntity } from './../../shared';

export class CSGOPlayerUrl implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public statsUrl?: string,
        public countryUrl?: string,
        public photoUrl?: string,
        public playerId?: number,
    ) {
    }
}
