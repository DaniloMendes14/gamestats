import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CSGOPlayerUrlComponent } from './csgo-player-url.component';
import { CSGOPlayerUrlDetailComponent } from './csgo-player-url-detail.component';
import { CSGOPlayerUrlPopupComponent } from './csgo-player-url-dialog.component';
import { CSGOPlayerUrlDeletePopupComponent } from './csgo-player-url-delete-dialog.component';

export const cSGOPlayerUrlRoute: Routes = [
    {
        path: 'csgo-player-url',
        component: CSGOPlayerUrlComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOPlayerUrl.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'csgo-player-url/:id',
        component: CSGOPlayerUrlDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOPlayerUrl.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cSGOPlayerUrlPopupRoute: Routes = [
    {
        path: 'csgo-player-url-new',
        component: CSGOPlayerUrlPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOPlayerUrl.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'csgo-player-url/:id/edit',
        component: CSGOPlayerUrlPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOPlayerUrl.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'csgo-player-url/:id/delete',
        component: CSGOPlayerUrlDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOPlayerUrl.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
