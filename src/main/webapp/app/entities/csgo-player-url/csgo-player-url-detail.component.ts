import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CSGOPlayerUrl } from './csgo-player-url.model';
import { CSGOPlayerUrlService } from './csgo-player-url.service';

@Component({
    selector: 'jhi-csgo-player-url-detail',
    templateUrl: './csgo-player-url-detail.component.html'
})
export class CSGOPlayerUrlDetailComponent implements OnInit, OnDestroy {

    cSGOPlayerUrl: CSGOPlayerUrl;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private cSGOPlayerUrlService: CSGOPlayerUrlService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCSGOPlayerUrls();
    }

    load(id) {
        this.cSGOPlayerUrlService.find(id).subscribe((cSGOPlayerUrl) => {
            this.cSGOPlayerUrl = cSGOPlayerUrl;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCSGOPlayerUrls() {
        this.eventSubscriber = this.eventManager.subscribe(
            'cSGOPlayerUrlListModification',
            (response) => this.load(this.cSGOPlayerUrl.id)
        );
    }
}
