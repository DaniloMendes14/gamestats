import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GamestatsSharedModule } from '../../shared';
import {
    CSGOPlayerUrlService,
    CSGOPlayerUrlPopupService,
    CSGOPlayerUrlComponent,
    CSGOPlayerUrlDetailComponent,
    CSGOPlayerUrlDialogComponent,
    CSGOPlayerUrlPopupComponent,
    CSGOPlayerUrlDeletePopupComponent,
    CSGOPlayerUrlDeleteDialogComponent,
    cSGOPlayerUrlRoute,
    cSGOPlayerUrlPopupRoute,
} from './';

const ENTITY_STATES = [
    ...cSGOPlayerUrlRoute,
    ...cSGOPlayerUrlPopupRoute,
];

@NgModule({
    imports: [
        GamestatsSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CSGOPlayerUrlComponent,
        CSGOPlayerUrlDetailComponent,
        CSGOPlayerUrlDialogComponent,
        CSGOPlayerUrlDeleteDialogComponent,
        CSGOPlayerUrlPopupComponent,
        CSGOPlayerUrlDeletePopupComponent,
    ],
    entryComponents: [
        CSGOPlayerUrlComponent,
        CSGOPlayerUrlDialogComponent,
        CSGOPlayerUrlPopupComponent,
        CSGOPlayerUrlDeleteDialogComponent,
        CSGOPlayerUrlDeletePopupComponent,
    ],
    providers: [
        CSGOPlayerUrlService,
        CSGOPlayerUrlPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GamestatsCSGOPlayerUrlModule {}
