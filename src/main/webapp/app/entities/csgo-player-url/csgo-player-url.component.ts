import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CSGOPlayerUrl } from './csgo-player-url.model';
import { CSGOPlayerUrlService } from './csgo-player-url.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-csgo-player-url',
    templateUrl: './csgo-player-url.component.html'
})
export class CSGOPlayerUrlComponent implements OnInit, OnDestroy {
cSGOPlayerUrls: CSGOPlayerUrl[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private cSGOPlayerUrlService: CSGOPlayerUrlService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.cSGOPlayerUrlService.query().subscribe(
            (res: ResponseWrapper) => {
                this.cSGOPlayerUrls = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCSGOPlayerUrls();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CSGOPlayerUrl) {
        return item.id;
    }
    registerChangeInCSGOPlayerUrls() {
        this.eventSubscriber = this.eventManager.subscribe('cSGOPlayerUrlListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
