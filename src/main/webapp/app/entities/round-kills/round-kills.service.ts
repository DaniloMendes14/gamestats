import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { RoundKills } from './round-kills.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RoundKillsService {

    private resourceUrl =  SERVER_API_URL + 'api/round-kills';

    constructor(private http: Http) { }

    create(roundKills: RoundKills): Observable<RoundKills> {
        const copy = this.convert(roundKills);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(roundKills: RoundKills): Observable<RoundKills> {
        const copy = this.convert(roundKills);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<RoundKills> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to RoundKills.
     */
    private convertItemFromServer(json: any): RoundKills {
        const entity: RoundKills = Object.assign(new RoundKills(), json);
        return entity;
    }

    /**
     * Convert a RoundKills to a JSON which can be sent to the server.
     */
    private convert(roundKills: RoundKills): RoundKills {
        const copy: RoundKills = Object.assign({}, roundKills);
        return copy;
    }
}
