import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GamestatsSharedModule } from '../../shared';
import {
    RoundKillsService,
    RoundKillsPopupService,
    RoundKillsComponent,
    RoundKillsDetailComponent,
    RoundKillsDialogComponent,
    RoundKillsPopupComponent,
    RoundKillsDeletePopupComponent,
    RoundKillsDeleteDialogComponent,
    roundKillsRoute,
    roundKillsPopupRoute,
} from './';

const ENTITY_STATES = [
    ...roundKillsRoute,
    ...roundKillsPopupRoute,
];

@NgModule({
    imports: [
        GamestatsSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        RoundKillsComponent,
        RoundKillsDetailComponent,
        RoundKillsDialogComponent,
        RoundKillsDeleteDialogComponent,
        RoundKillsPopupComponent,
        RoundKillsDeletePopupComponent,
    ],
    entryComponents: [
        RoundKillsComponent,
        RoundKillsDialogComponent,
        RoundKillsPopupComponent,
        RoundKillsDeleteDialogComponent,
        RoundKillsDeletePopupComponent,
    ],
    providers: [
        RoundKillsService,
        RoundKillsPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GamestatsRoundKillsModule {}
