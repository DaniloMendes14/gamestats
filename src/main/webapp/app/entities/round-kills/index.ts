export * from './round-kills.model';
export * from './round-kills-popup.service';
export * from './round-kills.service';
export * from './round-kills-dialog.component';
export * from './round-kills-delete-dialog.component';
export * from './round-kills-detail.component';
export * from './round-kills.component';
export * from './round-kills.route';
