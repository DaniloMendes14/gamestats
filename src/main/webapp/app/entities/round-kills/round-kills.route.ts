import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { RoundKillsComponent } from './round-kills.component';
import { RoundKillsDetailComponent } from './round-kills-detail.component';
import { RoundKillsPopupComponent } from './round-kills-dialog.component';
import { RoundKillsDeletePopupComponent } from './round-kills-delete-dialog.component';

export const roundKillsRoute: Routes = [
    {
        path: 'round-kills',
        component: RoundKillsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.roundKills.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'round-kills/:id',
        component: RoundKillsDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.roundKills.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const roundKillsPopupRoute: Routes = [
    {
        path: 'round-kills-new',
        component: RoundKillsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.roundKills.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'round-kills/:id/edit',
        component: RoundKillsPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.roundKills.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'round-kills/:id/delete',
        component: RoundKillsDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.roundKills.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
