import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RoundKills } from './round-kills.model';
import { RoundKillsPopupService } from './round-kills-popup.service';
import { RoundKillsService } from './round-kills.service';

@Component({
    selector: 'jhi-round-kills-delete-dialog',
    templateUrl: './round-kills-delete-dialog.component.html'
})
export class RoundKillsDeleteDialogComponent {

    roundKills: RoundKills;

    constructor(
        private roundKillsService: RoundKillsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.roundKillsService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'roundKillsListModification',
                content: 'Deleted an roundKills'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-round-kills-delete-popup',
    template: ''
})
export class RoundKillsDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private roundKillsPopupService: RoundKillsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.roundKillsPopupService
                .open(RoundKillsDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
