import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { RoundKills } from './round-kills.model';
import { RoundKillsService } from './round-kills.service';

@Component({
    selector: 'jhi-round-kills-detail',
    templateUrl: './round-kills-detail.component.html'
})
export class RoundKillsDetailComponent implements OnInit, OnDestroy {

    roundKills: RoundKills;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private roundKillsService: RoundKillsService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRoundKills();
    }

    load(id) {
        this.roundKillsService.find(id).subscribe((roundKills) => {
            this.roundKills = roundKills;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRoundKills() {
        this.eventSubscriber = this.eventManager.subscribe(
            'roundKillsListModification',
            (response) => this.load(this.roundKills.id)
        );
    }
}
