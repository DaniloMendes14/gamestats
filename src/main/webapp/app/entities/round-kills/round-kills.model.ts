import { BaseEntity } from './../../shared';

export class RoundKills implements BaseEntity {
    constructor(
        public id?: number,
        public noKill?: number,
        public oneKill?: number,
        public twoKills?: number,
        public threeKills?: number,
        public fourKills?: number,
        public fiveKills?: number,
    ) {
    }
}
