import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RoundKills } from './round-kills.model';
import { RoundKillsService } from './round-kills.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-round-kills',
    templateUrl: './round-kills.component.html'
})
export class RoundKillsComponent implements OnInit, OnDestroy {
roundKills: RoundKills[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private roundKillsService: RoundKillsService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.roundKillsService.query().subscribe(
            (res: ResponseWrapper) => {
                this.roundKills = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInRoundKills();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: RoundKills) {
        return item.id;
    }
    registerChangeInRoundKills() {
        this.eventSubscriber = this.eventManager.subscribe('roundKillsListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
