import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RoundKills } from './round-kills.model';
import { RoundKillsPopupService } from './round-kills-popup.service';
import { RoundKillsService } from './round-kills.service';

@Component({
    selector: 'jhi-round-kills-dialog',
    templateUrl: './round-kills-dialog.component.html'
})
export class RoundKillsDialogComponent implements OnInit {

    roundKills: RoundKills;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private roundKillsService: RoundKillsService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.roundKills.id !== undefined) {
            this.subscribeToSaveResponse(
                this.roundKillsService.update(this.roundKills));
        } else {
            this.subscribeToSaveResponse(
                this.roundKillsService.create(this.roundKills));
        }
    }

    private subscribeToSaveResponse(result: Observable<RoundKills>) {
        result.subscribe((res: RoundKills) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: RoundKills) {
        this.eventManager.broadcast({ name: 'roundKillsListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-round-kills-popup',
    template: ''
})
export class RoundKillsPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private roundKillsPopupService: RoundKillsPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.roundKillsPopupService
                    .open(RoundKillsDialogComponent as Component, params['id']);
            } else {
                this.roundKillsPopupService
                    .open(RoundKillsDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
