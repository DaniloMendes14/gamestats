import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { GamestatsCSGOPlayerModule } from './csgo-player/csgo-player.module';
import { GamestatsAchievementModule } from './achievement/achievement.module';
import { GamestatsRoundKillsModule } from './round-kills/round-kills.module';
import { GamestatsMatchModule } from './match/match.module';
import { GamestatsCSGOTeamModule } from './csgo-team/csgo-team.module';
import { GamestatsMapModule } from './map/map.module';
import { GamestatsCountryModule } from './country/country.module';
import { GamestatsCSGOPlayerUrlModule } from './csgo-player-url/csgo-player-url.module';
import { GamestatsCSGOTeamUrlModule } from './csgo-team-url/csgo-team-url.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        GamestatsCSGOPlayerModule,
        GamestatsAchievementModule,
        GamestatsRoundKillsModule,
        GamestatsMatchModule,
        GamestatsCSGOTeamModule,
        GamestatsMapModule,
        GamestatsCountryModule,
        GamestatsCSGOPlayerUrlModule,
        GamestatsCSGOTeamUrlModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GamestatsEntityModule {}
