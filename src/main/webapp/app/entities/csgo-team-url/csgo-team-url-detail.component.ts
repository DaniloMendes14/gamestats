import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CSGOTeamUrl } from './csgo-team-url.model';
import { CSGOTeamUrlService } from './csgo-team-url.service';

@Component({
    selector: 'jhi-csgo-team-url-detail',
    templateUrl: './csgo-team-url-detail.component.html'
})
export class CSGOTeamUrlDetailComponent implements OnInit, OnDestroy {

    cSGOTeamUrl: CSGOTeamUrl;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private cSGOTeamUrlService: CSGOTeamUrlService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCSGOTeamUrls();
    }

    load(id) {
        this.cSGOTeamUrlService.find(id).subscribe((cSGOTeamUrl) => {
            this.cSGOTeamUrl = cSGOTeamUrl;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCSGOTeamUrls() {
        this.eventSubscriber = this.eventManager.subscribe(
            'cSGOTeamUrlListModification',
            (response) => this.load(this.cSGOTeamUrl.id)
        );
    }
}
