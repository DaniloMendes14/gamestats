import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CSGOTeamUrl } from './csgo-team-url.model';
import { CSGOTeamUrlService } from './csgo-team-url.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-csgo-team-url',
    templateUrl: './csgo-team-url.component.html'
})
export class CSGOTeamUrlComponent implements OnInit, OnDestroy {
cSGOTeamUrls: CSGOTeamUrl[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private cSGOTeamUrlService: CSGOTeamUrlService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.cSGOTeamUrlService.query().subscribe(
            (res: ResponseWrapper) => {
                this.cSGOTeamUrls = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCSGOTeamUrls();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CSGOTeamUrl) {
        return item.id;
    }
    registerChangeInCSGOTeamUrls() {
        this.eventSubscriber = this.eventManager.subscribe('cSGOTeamUrlListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
