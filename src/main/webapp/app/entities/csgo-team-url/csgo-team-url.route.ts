import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CSGOTeamUrlComponent } from './csgo-team-url.component';
import { CSGOTeamUrlDetailComponent } from './csgo-team-url-detail.component';
import { CSGOTeamUrlPopupComponent } from './csgo-team-url-dialog.component';
import { CSGOTeamUrlDeletePopupComponent } from './csgo-team-url-delete-dialog.component';

export const cSGOTeamUrlRoute: Routes = [
    {
        path: 'csgo-team-url',
        component: CSGOTeamUrlComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOTeamUrl.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'csgo-team-url/:id',
        component: CSGOTeamUrlDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOTeamUrl.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cSGOTeamUrlPopupRoute: Routes = [
    {
        path: 'csgo-team-url-new',
        component: CSGOTeamUrlPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOTeamUrl.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'csgo-team-url/:id/edit',
        component: CSGOTeamUrlPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOTeamUrl.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'csgo-team-url/:id/delete',
        component: CSGOTeamUrlDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gamestatsApp.cSGOTeamUrl.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
