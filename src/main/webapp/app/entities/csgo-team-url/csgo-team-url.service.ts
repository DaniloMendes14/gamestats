import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { CSGOTeamUrl } from './csgo-team-url.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CSGOTeamUrlService {

    private resourceUrl =  SERVER_API_URL + 'api/csgo-team-urls';

    constructor(private http: Http) { }

    create(cSGOTeamUrl: CSGOTeamUrl): Observable<CSGOTeamUrl> {
        const copy = this.convert(cSGOTeamUrl);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(cSGOTeamUrl: CSGOTeamUrl): Observable<CSGOTeamUrl> {
        const copy = this.convert(cSGOTeamUrl);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<CSGOTeamUrl> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to CSGOTeamUrl.
     */
    private convertItemFromServer(json: any): CSGOTeamUrl {
        const entity: CSGOTeamUrl = Object.assign(new CSGOTeamUrl(), json);
        return entity;
    }

    /**
     * Convert a CSGOTeamUrl to a JSON which can be sent to the server.
     */
    private convert(cSGOTeamUrl: CSGOTeamUrl): CSGOTeamUrl {
        const copy: CSGOTeamUrl = Object.assign({}, cSGOTeamUrl);
        return copy;
    }
}
