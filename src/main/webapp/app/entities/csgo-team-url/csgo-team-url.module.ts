import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GamestatsSharedModule } from '../../shared';
import {
    CSGOTeamUrlService,
    CSGOTeamUrlPopupService,
    CSGOTeamUrlComponent,
    CSGOTeamUrlDetailComponent,
    CSGOTeamUrlDialogComponent,
    CSGOTeamUrlPopupComponent,
    CSGOTeamUrlDeletePopupComponent,
    CSGOTeamUrlDeleteDialogComponent,
    cSGOTeamUrlRoute,
    cSGOTeamUrlPopupRoute,
} from './';

const ENTITY_STATES = [
    ...cSGOTeamUrlRoute,
    ...cSGOTeamUrlPopupRoute,
];

@NgModule({
    imports: [
        GamestatsSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CSGOTeamUrlComponent,
        CSGOTeamUrlDetailComponent,
        CSGOTeamUrlDialogComponent,
        CSGOTeamUrlDeleteDialogComponent,
        CSGOTeamUrlPopupComponent,
        CSGOTeamUrlDeletePopupComponent,
    ],
    entryComponents: [
        CSGOTeamUrlComponent,
        CSGOTeamUrlDialogComponent,
        CSGOTeamUrlPopupComponent,
        CSGOTeamUrlDeleteDialogComponent,
        CSGOTeamUrlDeletePopupComponent,
    ],
    providers: [
        CSGOTeamUrlService,
        CSGOTeamUrlPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GamestatsCSGOTeamUrlModule {}
