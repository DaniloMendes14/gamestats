import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CSGOTeamUrl } from './csgo-team-url.model';
import { CSGOTeamUrlService } from './csgo-team-url.service';

@Injectable()
export class CSGOTeamUrlPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private cSGOTeamUrlService: CSGOTeamUrlService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.cSGOTeamUrlService.find(id).subscribe((cSGOTeamUrl) => {
                    this.ngbModalRef = this.cSGOTeamUrlModalRef(component, cSGOTeamUrl);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.cSGOTeamUrlModalRef(component, new CSGOTeamUrl());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    cSGOTeamUrlModalRef(component: Component, cSGOTeamUrl: CSGOTeamUrl): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.cSGOTeamUrl = cSGOTeamUrl;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
