import { BaseEntity } from './../../shared';

export class CSGOTeamUrl implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public statsUrl?: string,
        public photoUrl?: string,
        public teamId?: number,
    ) {
    }
}
