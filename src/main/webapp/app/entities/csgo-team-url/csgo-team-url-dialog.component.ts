import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CSGOTeamUrl } from './csgo-team-url.model';
import { CSGOTeamUrlPopupService } from './csgo-team-url-popup.service';
import { CSGOTeamUrlService } from './csgo-team-url.service';

@Component({
    selector: 'jhi-csgo-team-url-dialog',
    templateUrl: './csgo-team-url-dialog.component.html'
})
export class CSGOTeamUrlDialogComponent implements OnInit {

    cSGOTeamUrl: CSGOTeamUrl;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private cSGOTeamUrlService: CSGOTeamUrlService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.cSGOTeamUrl.id !== undefined) {
            this.subscribeToSaveResponse(
                this.cSGOTeamUrlService.update(this.cSGOTeamUrl));
        } else {
            this.subscribeToSaveResponse(
                this.cSGOTeamUrlService.create(this.cSGOTeamUrl));
        }
    }

    private subscribeToSaveResponse(result: Observable<CSGOTeamUrl>) {
        result.subscribe((res: CSGOTeamUrl) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CSGOTeamUrl) {
        this.eventManager.broadcast({ name: 'cSGOTeamUrlListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-csgo-team-url-popup',
    template: ''
})
export class CSGOTeamUrlPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cSGOTeamUrlPopupService: CSGOTeamUrlPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.cSGOTeamUrlPopupService
                    .open(CSGOTeamUrlDialogComponent as Component, params['id']);
            } else {
                this.cSGOTeamUrlPopupService
                    .open(CSGOTeamUrlDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
