import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CSGOTeamUrl } from './csgo-team-url.model';
import { CSGOTeamUrlPopupService } from './csgo-team-url-popup.service';
import { CSGOTeamUrlService } from './csgo-team-url.service';

@Component({
    selector: 'jhi-csgo-team-url-delete-dialog',
    templateUrl: './csgo-team-url-delete-dialog.component.html'
})
export class CSGOTeamUrlDeleteDialogComponent {

    cSGOTeamUrl: CSGOTeamUrl;

    constructor(
        private cSGOTeamUrlService: CSGOTeamUrlService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.cSGOTeamUrlService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'cSGOTeamUrlListModification',
                content: 'Deleted an cSGOTeamUrl'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-csgo-team-url-delete-popup',
    template: ''
})
export class CSGOTeamUrlDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cSGOTeamUrlPopupService: CSGOTeamUrlPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.cSGOTeamUrlPopupService
                .open(CSGOTeamUrlDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
