import { BaseEntity } from './../../shared';

export class Match implements BaseEntity {
    constructor(
        public id?: number,
        public date?: any,
        public numberOfMaps?: number,
        public championship?: string,
        public teamA?: BaseEntity,
        public teamB?: BaseEntity,
        public winner?: BaseEntity,
        public maps?: BaseEntity[],
    ) {
    }
}
