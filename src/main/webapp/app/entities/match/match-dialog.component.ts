import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Match } from './match.model';
import { MatchPopupService } from './match-popup.service';
import { MatchService } from './match.service';
import { CSGOTeam, CSGOTeamService } from '../csgo-team';
import { Map, MapService } from '../map';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-match-dialog',
    templateUrl: './match-dialog.component.html'
})
export class MatchDialogComponent implements OnInit {

    match: Match;
    isSaving: boolean;

    teamas: CSGOTeam[];

    teambs: CSGOTeam[];

    winners: CSGOTeam[];

    maps: Map[];
    dateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private matchService: MatchService,
        private cSGOTeamService: CSGOTeamService,
        private mapService: MapService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.cSGOTeamService
            .query({filter: 'match-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.match.teamA || !this.match.teamA.id) {
                    this.teamas = res.json;
                } else {
                    this.cSGOTeamService
                        .find(this.match.teamA.id)
                        .subscribe((subRes: CSGOTeam) => {
                            this.teamas = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.cSGOTeamService
            .query({filter: 'match-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.match.teamB || !this.match.teamB.id) {
                    this.teambs = res.json;
                } else {
                    this.cSGOTeamService
                        .find(this.match.teamB.id)
                        .subscribe((subRes: CSGOTeam) => {
                            this.teambs = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.cSGOTeamService
            .query({filter: 'match-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.match.winner || !this.match.winner.id) {
                    this.winners = res.json;
                } else {
                    this.cSGOTeamService
                        .find(this.match.winner.id)
                        .subscribe((subRes: CSGOTeam) => {
                            this.winners = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        this.mapService.query()
            .subscribe((res: ResponseWrapper) => { this.maps = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.match.id !== undefined) {
            this.subscribeToSaveResponse(
                this.matchService.update(this.match));
        } else {
            this.subscribeToSaveResponse(
                this.matchService.create(this.match));
        }
    }

    private subscribeToSaveResponse(result: Observable<Match>) {
        result.subscribe((res: Match) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Match) {
        this.eventManager.broadcast({ name: 'matchListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCSGOTeamById(index: number, item: CSGOTeam) {
        return item.id;
    }

    trackMapById(index: number, item: Map) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-match-popup',
    template: ''
})
export class MatchPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private matchPopupService: MatchPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.matchPopupService
                    .open(MatchDialogComponent as Component, params['id']);
            } else {
                this.matchPopupService
                    .open(MatchDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
