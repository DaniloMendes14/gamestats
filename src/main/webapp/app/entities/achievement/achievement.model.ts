import { BaseEntity } from './../../shared';

export class Achievement implements BaseEntity {
    constructor(
        public id?: number,
        public date?: any,
        public description?: string,
        public team?: BaseEntity,
        public player?: BaseEntity,
    ) {
    }
}
