package com.danilomendes.gamestats.config;

import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class QuartzConfig extends SchedulerFactoryBean {

    static Logger LOGGER = Logger.getLogger(QuartzConfig.class.getName());

    @Override
    public void afterPropertiesSet() throws Exception {
        super.afterPropertiesSet();
        LOGGER.info("ATIVADAS - Tasks agendadas pelo Spring com o Quartz em applicationContext-quartz.xml");
    }
}
