package com.danilomendes.gamestats.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.danilomendes.gamestats.domain.CSGOTeam;

import com.danilomendes.gamestats.repository.CSGOTeamRepository;
import com.danilomendes.gamestats.web.rest.errors.BadRequestAlertException;
import com.danilomendes.gamestats.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CSGOTeam.
 */
@RestController
@RequestMapping("/api")
public class CSGOTeamResource {

    private final Logger log = LoggerFactory.getLogger(CSGOTeamResource.class);

    private static final String ENTITY_NAME = "cSGOTeam";

    private final CSGOTeamRepository cSGOTeamRepository;

    public CSGOTeamResource(CSGOTeamRepository cSGOTeamRepository) {
        this.cSGOTeamRepository = cSGOTeamRepository;
    }

    /**
     * POST  /csgo-teams : Create a new cSGOTeam.
     *
     * @param cSGOTeam the cSGOTeam to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cSGOTeam, or with status 400 (Bad Request) if the cSGOTeam has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/csgo-teams")
    @Timed
    public ResponseEntity<CSGOTeam> createCSGOTeam(@RequestBody CSGOTeam cSGOTeam) throws URISyntaxException {
        log.debug("REST request to save CSGOTeam : {}", cSGOTeam);
        if (cSGOTeam.getId() != null) {
            throw new BadRequestAlertException("A new cSGOTeam cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CSGOTeam result = cSGOTeamRepository.save(cSGOTeam);
        return ResponseEntity.created(new URI("/api/csgo-teams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /csgo-teams : Updates an existing cSGOTeam.
     *
     * @param cSGOTeam the cSGOTeam to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cSGOTeam,
     * or with status 400 (Bad Request) if the cSGOTeam is not valid,
     * or with status 500 (Internal Server Error) if the cSGOTeam couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/csgo-teams")
    @Timed
    public ResponseEntity<CSGOTeam> updateCSGOTeam(@RequestBody CSGOTeam cSGOTeam) throws URISyntaxException {
        log.debug("REST request to update CSGOTeam : {}", cSGOTeam);
        if (cSGOTeam.getId() == null) {
            return createCSGOTeam(cSGOTeam);
        }
        CSGOTeam result = cSGOTeamRepository.save(cSGOTeam);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cSGOTeam.getId().toString()))
            .body(result);
    }

    /**
     * GET  /csgo-teams : get all the cSGOTeams.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cSGOTeams in body
     */
    @GetMapping("/csgo-teams")
    @Timed
    public List<CSGOTeam> getAllCSGOTeams() {
        log.debug("REST request to get all CSGOTeams");
        return cSGOTeamRepository.findAll();
        }

    /**
     * GET  /csgo-teams/:id : get the "id" cSGOTeam.
     *
     * @param id the id of the cSGOTeam to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cSGOTeam, or with status 404 (Not Found)
     */
    @GetMapping("/csgo-teams/{id}")
    @Timed
    public ResponseEntity<CSGOTeam> getCSGOTeam(@PathVariable Long id) {
        log.debug("REST request to get CSGOTeam : {}", id);
        CSGOTeam cSGOTeam = cSGOTeamRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cSGOTeam));
    }

    /**
     * DELETE  /csgo-teams/:id : delete the "id" cSGOTeam.
     *
     * @param id the id of the cSGOTeam to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/csgo-teams/{id}")
    @Timed
    public ResponseEntity<Void> deleteCSGOTeam(@PathVariable Long id) {
        log.debug("REST request to delete CSGOTeam : {}", id);
        cSGOTeamRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
