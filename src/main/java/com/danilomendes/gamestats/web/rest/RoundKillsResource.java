package com.danilomendes.gamestats.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.danilomendes.gamestats.domain.RoundKills;

import com.danilomendes.gamestats.repository.RoundKillsRepository;
import com.danilomendes.gamestats.web.rest.errors.BadRequestAlertException;
import com.danilomendes.gamestats.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RoundKills.
 */
@RestController
@RequestMapping("/api")
public class RoundKillsResource {

    private final Logger log = LoggerFactory.getLogger(RoundKillsResource.class);

    private static final String ENTITY_NAME = "roundKills";

    private final RoundKillsRepository roundKillsRepository;

    public RoundKillsResource(RoundKillsRepository roundKillsRepository) {
        this.roundKillsRepository = roundKillsRepository;
    }

    /**
     * POST  /round-kills : Create a new roundKills.
     *
     * @param roundKills the roundKills to create
     * @return the ResponseEntity with status 201 (Created) and with body the new roundKills, or with status 400 (Bad Request) if the roundKills has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/round-kills")
    @Timed
    public ResponseEntity<RoundKills> createRoundKills(@RequestBody RoundKills roundKills) throws URISyntaxException {
        log.debug("REST request to save RoundKills : {}", roundKills);
        if (roundKills.getId() != null) {
            throw new BadRequestAlertException("A new roundKills cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RoundKills result = roundKillsRepository.save(roundKills);
        return ResponseEntity.created(new URI("/api/round-kills/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /round-kills : Updates an existing roundKills.
     *
     * @param roundKills the roundKills to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated roundKills,
     * or with status 400 (Bad Request) if the roundKills is not valid,
     * or with status 500 (Internal Server Error) if the roundKills couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/round-kills")
    @Timed
    public ResponseEntity<RoundKills> updateRoundKills(@RequestBody RoundKills roundKills) throws URISyntaxException {
        log.debug("REST request to update RoundKills : {}", roundKills);
        if (roundKills.getId() == null) {
            return createRoundKills(roundKills);
        }
        RoundKills result = roundKillsRepository.save(roundKills);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, roundKills.getId().toString()))
            .body(result);
    }

    /**
     * GET  /round-kills : get all the roundKills.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of roundKills in body
     */
    @GetMapping("/round-kills")
    @Timed
    public List<RoundKills> getAllRoundKills() {
        log.debug("REST request to get all RoundKills");
        return roundKillsRepository.findAll();
        }

    /**
     * GET  /round-kills/:id : get the "id" roundKills.
     *
     * @param id the id of the roundKills to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the roundKills, or with status 404 (Not Found)
     */
    @GetMapping("/round-kills/{id}")
    @Timed
    public ResponseEntity<RoundKills> getRoundKills(@PathVariable Long id) {
        log.debug("REST request to get RoundKills : {}", id);
        RoundKills roundKills = roundKillsRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(roundKills));
    }

    /**
     * DELETE  /round-kills/:id : delete the "id" roundKills.
     *
     * @param id the id of the roundKills to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/round-kills/{id}")
    @Timed
    public ResponseEntity<Void> deleteRoundKills(@PathVariable Long id) {
        log.debug("REST request to delete RoundKills : {}", id);
        roundKillsRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
