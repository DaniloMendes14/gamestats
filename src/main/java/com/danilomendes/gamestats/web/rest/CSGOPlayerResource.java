package com.danilomendes.gamestats.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.danilomendes.gamestats.domain.CSGOPlayer;

import com.danilomendes.gamestats.repository.CSGOPlayerRepository;
import com.danilomendes.gamestats.web.rest.errors.BadRequestAlertException;
import com.danilomendes.gamestats.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CSGOPlayer.
 */
@RestController
@RequestMapping("/api")
public class CSGOPlayerResource {

    private final Logger log = LoggerFactory.getLogger(CSGOPlayerResource.class);

    private static final String ENTITY_NAME = "cSGOPlayer";

    private final CSGOPlayerRepository cSGOPlayerRepository;

    public CSGOPlayerResource(CSGOPlayerRepository cSGOPlayerRepository) {
        this.cSGOPlayerRepository = cSGOPlayerRepository;
    }

    /**
     * POST  /csgo-players : Create a new cSGOPlayer.
     *
     * @param cSGOPlayer the cSGOPlayer to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cSGOPlayer, or with status 400 (Bad Request) if the cSGOPlayer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/csgo-players")
    @Timed
    public ResponseEntity<CSGOPlayer> createCSGOPlayer(@RequestBody CSGOPlayer cSGOPlayer) throws URISyntaxException {
        log.debug("REST request to save CSGOPlayer : {}", cSGOPlayer);
        if (cSGOPlayer.getId() != null) {
            throw new BadRequestAlertException("A new cSGOPlayer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CSGOPlayer result = cSGOPlayerRepository.save(cSGOPlayer);
        return ResponseEntity.created(new URI("/api/csgo-players/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /csgo-players : Updates an existing cSGOPlayer.
     *
     * @param cSGOPlayer the cSGOPlayer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cSGOPlayer,
     * or with status 400 (Bad Request) if the cSGOPlayer is not valid,
     * or with status 500 (Internal Server Error) if the cSGOPlayer couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/csgo-players")
    @Timed
    public ResponseEntity<CSGOPlayer> updateCSGOPlayer(@RequestBody CSGOPlayer cSGOPlayer) throws URISyntaxException {
        log.debug("REST request to update CSGOPlayer : {}", cSGOPlayer);
        if (cSGOPlayer.getId() == null) {
            return createCSGOPlayer(cSGOPlayer);
        }
        CSGOPlayer result = cSGOPlayerRepository.save(cSGOPlayer);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cSGOPlayer.getId().toString()))
            .body(result);
    }

    /**
     * GET  /csgo-players : get all the cSGOPlayers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cSGOPlayers in body
     */
    @GetMapping("/csgo-players")
    @Timed
    public List<CSGOPlayer> getAllCSGOPlayers() {
        log.debug("REST request to get all CSGOPlayers");
        return cSGOPlayerRepository.findAll();
        }

    /**
     * GET  /csgo-players/:id : get the "id" cSGOPlayer.
     *
     * @param id the id of the cSGOPlayer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cSGOPlayer, or with status 404 (Not Found)
     */
    @GetMapping("/csgo-players/{id}")
    @Timed
    public ResponseEntity<CSGOPlayer> getCSGOPlayer(@PathVariable Long id) {
        log.debug("REST request to get CSGOPlayer : {}", id);
        CSGOPlayer cSGOPlayer = cSGOPlayerRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cSGOPlayer));
    }

    /**
     * DELETE  /csgo-players/:id : delete the "id" cSGOPlayer.
     *
     * @param id the id of the cSGOPlayer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/csgo-players/{id}")
    @Timed
    public ResponseEntity<Void> deleteCSGOPlayer(@PathVariable Long id) {
        log.debug("REST request to delete CSGOPlayer : {}", id);
        cSGOPlayerRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
