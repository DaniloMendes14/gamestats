package com.danilomendes.gamestats.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.danilomendes.gamestats.domain.CSGOTeamUrl;
import com.danilomendes.gamestats.service.CSGOTeamUrlService;
import com.danilomendes.gamestats.web.rest.errors.BadRequestAlertException;
import com.danilomendes.gamestats.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CSGOTeamUrl.
 */
@RestController
@RequestMapping("/api")
public class CSGOTeamUrlResource {

    private final Logger log = LoggerFactory.getLogger(CSGOTeamUrlResource.class);

    private static final String ENTITY_NAME = "cSGOTeamUrl";

    private final CSGOTeamUrlService cSGOTeamUrlService;

    public CSGOTeamUrlResource(CSGOTeamUrlService cSGOTeamUrlService) {
        this.cSGOTeamUrlService = cSGOTeamUrlService;
    }

    /**
     * POST  /csgo-team-urls : Create a new cSGOTeamUrl.
     *
     * @param cSGOTeamUrl the cSGOTeamUrl to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cSGOTeamUrl, or with status 400 (Bad Request) if the cSGOTeamUrl has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/csgo-team-urls")
    @Timed
    public ResponseEntity<CSGOTeamUrl> createCSGOTeamUrl(@RequestBody CSGOTeamUrl cSGOTeamUrl) throws URISyntaxException {
        log.debug("REST request to save CSGOTeamUrl : {}", cSGOTeamUrl);
        if (cSGOTeamUrl.getId() != null) {
            throw new BadRequestAlertException("A new cSGOTeamUrl cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CSGOTeamUrl result = cSGOTeamUrlService.save(cSGOTeamUrl);
        return ResponseEntity.created(new URI("/api/csgo-team-urls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /csgo-team-urls : Updates an existing cSGOTeamUrl.
     *
     * @param cSGOTeamUrl the cSGOTeamUrl to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cSGOTeamUrl,
     * or with status 400 (Bad Request) if the cSGOTeamUrl is not valid,
     * or with status 500 (Internal Server Error) if the cSGOTeamUrl couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/csgo-team-urls")
    @Timed
    public ResponseEntity<CSGOTeamUrl> updateCSGOTeamUrl(@RequestBody CSGOTeamUrl cSGOTeamUrl) throws URISyntaxException {
        log.debug("REST request to update CSGOTeamUrl : {}", cSGOTeamUrl);
        if (cSGOTeamUrl.getId() == null) {
            return createCSGOTeamUrl(cSGOTeamUrl);
        }
        CSGOTeamUrl result = cSGOTeamUrlService.save(cSGOTeamUrl);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cSGOTeamUrl.getId().toString()))
            .body(result);
    }

    /**
     * GET  /csgo-team-urls : get all the cSGOTeamUrls.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cSGOTeamUrls in body
     */
    @GetMapping("/csgo-team-urls")
    @Timed
    public List<CSGOTeamUrl> getAllCSGOTeamUrls() {
        log.debug("REST request to get all CSGOTeamUrls");
        return cSGOTeamUrlService.findAll();
        }

    /**
     * GET  /csgo-team-urls/:id : get the "id" cSGOTeamUrl.
     *
     * @param id the id of the cSGOTeamUrl to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cSGOTeamUrl, or with status 404 (Not Found)
     */
    @GetMapping("/csgo-team-urls/{id}")
    @Timed
    public ResponseEntity<CSGOTeamUrl> getCSGOTeamUrl(@PathVariable Long id) {
        log.debug("REST request to get CSGOTeamUrl : {}", id);
        CSGOTeamUrl cSGOTeamUrl = cSGOTeamUrlService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cSGOTeamUrl));
    }

    /**
     * DELETE  /csgo-team-urls/:id : delete the "id" cSGOTeamUrl.
     *
     * @param id the id of the cSGOTeamUrl to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/csgo-team-urls/{id}")
    @Timed
    public ResponseEntity<Void> deleteCSGOTeamUrl(@PathVariable Long id) {
        log.debug("REST request to delete CSGOTeamUrl : {}", id);
        cSGOTeamUrlService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
