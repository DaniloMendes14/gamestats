package com.danilomendes.gamestats.web.rest;

import com.danilomendes.gamestats.service.PlayerInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * PlayerInfo controller
 */
@RestController
@RequestMapping("/api/player-info")
public class PlayerInfoResource {

    private final Logger log = LoggerFactory.getLogger(PlayerInfoResource.class);

    @Autowired
    private PlayerInfoService playerInfoService;

    /**
    * GET getPlayerInfo
    */
    @GetMapping("/get-player-info")
    public String getPlayerInfo() {
        playerInfoService.getPlayerInfo();
        return
            "getPlayerInfo";
    }


}
