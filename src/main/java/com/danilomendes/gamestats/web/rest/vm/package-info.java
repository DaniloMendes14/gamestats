/**
 * View Models used by Spring MVC REST controllers.
 */
package com.danilomendes.gamestats.web.rest.vm;
