package com.danilomendes.gamestats.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.danilomendes.gamestats.domain.CSGOPlayerUrl;

import com.danilomendes.gamestats.repository.CSGOPlayerUrlRepository;
import com.danilomendes.gamestats.web.rest.errors.BadRequestAlertException;
import com.danilomendes.gamestats.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CSGOPlayerUrl.
 */
@RestController
@RequestMapping("/api")
public class CSGOPlayerUrlResource {

    private final Logger log = LoggerFactory.getLogger(CSGOPlayerUrlResource.class);

    private static final String ENTITY_NAME = "cSGOPlayerUrl";

    private final CSGOPlayerUrlRepository cSGOPlayerUrlRepository;

    public CSGOPlayerUrlResource(CSGOPlayerUrlRepository cSGOPlayerUrlRepository) {
        this.cSGOPlayerUrlRepository = cSGOPlayerUrlRepository;
    }

    /**
     * POST  /csgo-player-urls : Create a new cSGOPlayerUrl.
     *
     * @param cSGOPlayerUrl the cSGOPlayerUrl to create
     * @return the ResponseEntity with status 201 (Created) and with body the new cSGOPlayerUrl, or with status 400 (Bad Request) if the cSGOPlayerUrl has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/csgo-player-urls")
    @Timed
    public ResponseEntity<CSGOPlayerUrl> createCSGOPlayerUrl(@RequestBody CSGOPlayerUrl cSGOPlayerUrl) throws URISyntaxException {
        log.debug("REST request to save CSGOPlayerUrl : {}", cSGOPlayerUrl);
        if (cSGOPlayerUrl.getId() != null) {
            throw new BadRequestAlertException("A new cSGOPlayerUrl cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CSGOPlayerUrl result = cSGOPlayerUrlRepository.save(cSGOPlayerUrl);
        return ResponseEntity.created(new URI("/api/csgo-player-urls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /csgo-player-urls : Updates an existing cSGOPlayerUrl.
     *
     * @param cSGOPlayerUrl the cSGOPlayerUrl to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated cSGOPlayerUrl,
     * or with status 400 (Bad Request) if the cSGOPlayerUrl is not valid,
     * or with status 500 (Internal Server Error) if the cSGOPlayerUrl couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/csgo-player-urls")
    @Timed
    public ResponseEntity<CSGOPlayerUrl> updateCSGOPlayerUrl(@RequestBody CSGOPlayerUrl cSGOPlayerUrl) throws URISyntaxException {
        log.debug("REST request to update CSGOPlayerUrl : {}", cSGOPlayerUrl);
        if (cSGOPlayerUrl.getId() == null) {
            return createCSGOPlayerUrl(cSGOPlayerUrl);
        }
        CSGOPlayerUrl result = cSGOPlayerUrlRepository.save(cSGOPlayerUrl);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cSGOPlayerUrl.getId().toString()))
            .body(result);
    }

    /**
     * GET  /csgo-player-urls : get all the cSGOPlayerUrls.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of cSGOPlayerUrls in body
     */
    @GetMapping("/csgo-player-urls")
    @Timed
    public List<CSGOPlayerUrl> getAllCSGOPlayerUrls() {
        log.debug("REST request to get all CSGOPlayerUrls");
        return cSGOPlayerUrlRepository.findAll();
        }

    /**
     * GET  /csgo-player-urls/:id : get the "id" cSGOPlayerUrl.
     *
     * @param id the id of the cSGOPlayerUrl to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the cSGOPlayerUrl, or with status 404 (Not Found)
     */
    @GetMapping("/csgo-player-urls/{id}")
    @Timed
    public ResponseEntity<CSGOPlayerUrl> getCSGOPlayerUrl(@PathVariable Long id) {
        log.debug("REST request to get CSGOPlayerUrl : {}", id);
        CSGOPlayerUrl cSGOPlayerUrl = cSGOPlayerUrlRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cSGOPlayerUrl));
    }

    /**
     * DELETE  /csgo-player-urls/:id : delete the "id" cSGOPlayerUrl.
     *
     * @param id the id of the cSGOPlayerUrl to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/csgo-player-urls/{id}")
    @Timed
    public ResponseEntity<Void> deleteCSGOPlayerUrl(@PathVariable Long id) {
        log.debug("REST request to delete CSGOPlayerUrl : {}", id);
        cSGOPlayerUrlRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
