package com.danilomendes.gamestats.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A CSGOPlayerUrl.
 */
@Entity
@Table(name = "csgoplayer_url")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CSGOPlayerUrl implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "stats_url")
    private String statsUrl;

    @Column(name = "country_url")
    private String countryUrl;

    @Column(name = "photo_url")
    private String photoUrl;

    @Column(name = "player_id")
    private Long playerId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CSGOPlayerUrl name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatsUrl() {
        return statsUrl;
    }

    public CSGOPlayerUrl statsUrl(String statsUrl) {
        this.statsUrl = statsUrl;
        return this;
    }

    public void setStatsUrl(String statsUrl) {
        this.statsUrl = statsUrl;
    }

    public String getCountryUrl() {
        return countryUrl;
    }

    public CSGOPlayerUrl countryUrl(String countryUrl) {
        this.countryUrl = countryUrl;
        return this;
    }

    public void setCountryUrl(String countryUrl) {
        this.countryUrl = countryUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public CSGOPlayerUrl photoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
        return this;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public CSGOPlayerUrl playerId(Long playerId) {
        this.playerId = playerId;
        return this;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CSGOPlayerUrl cSGOPlayerUrl = (CSGOPlayerUrl) o;
        if (cSGOPlayerUrl.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cSGOPlayerUrl.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CSGOPlayerUrl{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", statsUrl='" + getStatsUrl() + "'" +
            ", countryUrl='" + getCountryUrl() + "'" +
            ", photoUrl='" + getPhotoUrl() + "'" +
            ", playerId=" + getPlayerId() +
            "}";
    }
}
