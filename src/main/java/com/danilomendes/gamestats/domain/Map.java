package com.danilomendes.gamestats.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Map.
 */
@Entity
@Table(name = "map")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Map implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "users_rating")
    private Double usersRating;

    @Column(name = "ct_win_ratio")
    private Double ctWinRatio;

    @Column(name = "t_win_ratio")
    private Double tWinRatio;

    @Column(name = "number_played")
    private Long numberPlayed;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Map name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getUsersRating() {
        return usersRating;
    }

    public Map usersRating(Double usersRating) {
        this.usersRating = usersRating;
        return this;
    }

    public void setUsersRating(Double usersRating) {
        this.usersRating = usersRating;
    }

    public Double getCtWinRatio() {
        return ctWinRatio;
    }

    public Map ctWinRatio(Double ctWinRatio) {
        this.ctWinRatio = ctWinRatio;
        return this;
    }

    public void setCtWinRatio(Double ctWinRatio) {
        this.ctWinRatio = ctWinRatio;
    }

    public Double gettWinRatio() {
        return tWinRatio;
    }

    public Map tWinRatio(Double tWinRatio) {
        this.tWinRatio = tWinRatio;
        return this;
    }

    public void settWinRatio(Double tWinRatio) {
        this.tWinRatio = tWinRatio;
    }

    public Long getNumberPlayed() {
        return numberPlayed;
    }

    public Map numberPlayed(Long numberPlayed) {
        this.numberPlayed = numberPlayed;
        return this;
    }

    public void setNumberPlayed(Long numberPlayed) {
        this.numberPlayed = numberPlayed;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Map map = (Map) o;
        if (map.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), map.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Map{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", usersRating=" + getUsersRating() +
            ", ctWinRatio=" + getCtWinRatio() +
            ", tWinRatio=" + gettWinRatio() +
            ", numberPlayed=" + getNumberPlayed() +
            "}";
    }
}
