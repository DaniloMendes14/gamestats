package com.danilomendes.gamestats.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A CSGOTeam.
 */
@Entity
@Table(name = "csgo_team")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CSGOTeam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "rating")
    private Double rating;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "users_rating")
    private Double usersRating;

    @Column(name = "maps_played")
    private Long mapsPlayed;

    @Lob
    @Column(name = "players_photo")
    private byte[] playersPhoto;

    @Column(name = "players_photo_content_type")
    private String playersPhotoContentType;

    @Lob
    @Column(name = "team_logo")
    private byte[] teamLogo;

    @Column(name = "team_logo_content_type")
    private String teamLogoContentType;

    @Column(name = "ct_win_ratio")
    private Double ctWinRatio;

    @Column(name = "t_win_ratio")
    private Double tWinRatio;

    @Column(name = "number_of_maps_played")
    private Long numberOfMapsPlayed;

    @Column(name = "twitter_url")
    private String twitterUrl;

    @OneToOne
    @JoinColumn(unique = true)
    private Country country;

    @OneToOne
    @JoinColumn(unique = true)
    private Match lastMatch;

    @OneToOne
    @JoinColumn(unique = true)
    private RoundKills roundKills;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CSGOTeam name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getRating() {
        return rating;
    }

    public CSGOTeam rating(Double rating) {
        this.rating = rating;
        return this;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public CSGOTeam creationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Double getUsersRating() {
        return usersRating;
    }

    public CSGOTeam usersRating(Double usersRating) {
        this.usersRating = usersRating;
        return this;
    }

    public void setUsersRating(Double usersRating) {
        this.usersRating = usersRating;
    }

    public Long getMapsPlayed() {
        return mapsPlayed;
    }

    public CSGOTeam mapsPlayed(Long mapsPlayed) {
        this.mapsPlayed = mapsPlayed;
        return this;
    }

    public void setMapsPlayed(Long mapsPlayed) {
        this.mapsPlayed = mapsPlayed;
    }

    public byte[] getPlayersPhoto() {
        return playersPhoto;
    }

    public CSGOTeam playersPhoto(byte[] playersPhoto) {
        this.playersPhoto = playersPhoto;
        return this;
    }

    public void setPlayersPhoto(byte[] playersPhoto) {
        this.playersPhoto = playersPhoto;
    }

    public String getPlayersPhotoContentType() {
        return playersPhotoContentType;
    }

    public CSGOTeam playersPhotoContentType(String playersPhotoContentType) {
        this.playersPhotoContentType = playersPhotoContentType;
        return this;
    }

    public void setPlayersPhotoContentType(String playersPhotoContentType) {
        this.playersPhotoContentType = playersPhotoContentType;
    }

    public byte[] getTeamLogo() {
        return teamLogo;
    }

    public CSGOTeam teamLogo(byte[] teamLogo) {
        this.teamLogo = teamLogo;
        return this;
    }

    public void setTeamLogo(byte[] teamLogo) {
        this.teamLogo = teamLogo;
    }

    public String getTeamLogoContentType() {
        return teamLogoContentType;
    }

    public CSGOTeam teamLogoContentType(String teamLogoContentType) {
        this.teamLogoContentType = teamLogoContentType;
        return this;
    }

    public void setTeamLogoContentType(String teamLogoContentType) {
        this.teamLogoContentType = teamLogoContentType;
    }

    public Double getCtWinRatio() {
        return ctWinRatio;
    }

    public CSGOTeam ctWinRatio(Double ctWinRatio) {
        this.ctWinRatio = ctWinRatio;
        return this;
    }

    public void setCtWinRatio(Double ctWinRatio) {
        this.ctWinRatio = ctWinRatio;
    }

    public Double gettWinRatio() {
        return tWinRatio;
    }

    public CSGOTeam tWinRatio(Double tWinRatio) {
        this.tWinRatio = tWinRatio;
        return this;
    }

    public void settWinRatio(Double tWinRatio) {
        this.tWinRatio = tWinRatio;
    }

    public Long getNumberOfMapsPlayed() {
        return numberOfMapsPlayed;
    }

    public CSGOTeam numberOfMapsPlayed(Long numberOfMapsPlayed) {
        this.numberOfMapsPlayed = numberOfMapsPlayed;
        return this;
    }

    public void setNumberOfMapsPlayed(Long numberOfMapsPlayed) {
        this.numberOfMapsPlayed = numberOfMapsPlayed;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public CSGOTeam twitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
        return this;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    public Country getCountry() {
        return country;
    }

    public CSGOTeam country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Match getLastMatch() {
        return lastMatch;
    }

    public CSGOTeam lastMatch(Match match) {
        this.lastMatch = match;
        return this;
    }

    public void setLastMatch(Match match) {
        this.lastMatch = match;
    }

    public RoundKills getRoundKills() {
        return roundKills;
    }

    public CSGOTeam roundKills(RoundKills roundKills) {
        this.roundKills = roundKills;
        return this;
    }

    public void setRoundKills(RoundKills roundKills) {
        this.roundKills = roundKills;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CSGOTeam cSGOTeam = (CSGOTeam) o;
        if (cSGOTeam.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cSGOTeam.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CSGOTeam{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", rating=" + getRating() +
            ", creationDate='" + getCreationDate() + "'" +
            ", usersRating=" + getUsersRating() +
            ", mapsPlayed=" + getMapsPlayed() +
            ", playersPhoto='" + getPlayersPhoto() + "'" +
            ", playersPhotoContentType='" + getPlayersPhotoContentType() + "'" +
            ", teamLogo='" + getTeamLogo() + "'" +
            ", teamLogoContentType='" + getTeamLogoContentType() + "'" +
            ", ctWinRatio=" + getCtWinRatio() +
            ", tWinRatio=" + gettWinRatio() +
            ", numberOfMapsPlayed=" + getNumberOfMapsPlayed() +
            ", twitterUrl='" + getTwitterUrl() + "'" +
            "}";
    }
}
