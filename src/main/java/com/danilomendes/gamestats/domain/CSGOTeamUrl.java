package com.danilomendes.gamestats.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A CSGOTeamUrl.
 */
@Entity
@Table(name = "csgoteam_url")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CSGOTeamUrl implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "stats_url")
    private String statsUrl;

    @Column(name = "photo_url")
    private String photoUrl;

    @Column(name = "team_id")
    private Long teamId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CSGOTeamUrl name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatsUrl() {
        return statsUrl;
    }

    public CSGOTeamUrl statsUrl(String statsUrl) {
        this.statsUrl = statsUrl;
        return this;
    }

    public void setStatsUrl(String statsUrl) {
        this.statsUrl = statsUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public CSGOTeamUrl photoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
        return this;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Long getTeamId() {
        return teamId;
    }

    public CSGOTeamUrl teamId(Long teamId) {
        this.teamId = teamId;
        return this;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CSGOTeamUrl cSGOTeamUrl = (CSGOTeamUrl) o;
        if (cSGOTeamUrl.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cSGOTeamUrl.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CSGOTeamUrl{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", statsUrl='" + getStatsUrl() + "'" +
            ", photoUrl='" + getPhotoUrl() + "'" +
            ", teamId=" + getTeamId() +
            "}";
    }
}
