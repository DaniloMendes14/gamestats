package com.danilomendes.gamestats.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Match.
 */
@Entity
@Table(name = "match")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Match implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_date")
    private LocalDate date;

    @Column(name = "number_of_maps")
    private Long numberOfMaps;

    @Column(name = "championship")
    private String championship;

    @OneToOne
    @JoinColumn(unique = true)
    private CSGOTeam teamA;

    @OneToOne
    @JoinColumn(unique = true)
    private CSGOTeam teamB;

    @OneToOne
    @JoinColumn(unique = true)
    private CSGOTeam winner;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "match_maps",
               joinColumns = @JoinColumn(name="matches_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="maps_id", referencedColumnName="id"))
    private Set<Map> maps = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Match date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getNumberOfMaps() {
        return numberOfMaps;
    }

    public Match numberOfMaps(Long numberOfMaps) {
        this.numberOfMaps = numberOfMaps;
        return this;
    }

    public void setNumberOfMaps(Long numberOfMaps) {
        this.numberOfMaps = numberOfMaps;
    }

    public String getChampionship() {
        return championship;
    }

    public Match championship(String championship) {
        this.championship = championship;
        return this;
    }

    public void setChampionship(String championship) {
        this.championship = championship;
    }

    public CSGOTeam getTeamA() {
        return teamA;
    }

    public Match teamA(CSGOTeam cSGOTeam) {
        this.teamA = cSGOTeam;
        return this;
    }

    public void setTeamA(CSGOTeam cSGOTeam) {
        this.teamA = cSGOTeam;
    }

    public CSGOTeam getTeamB() {
        return teamB;
    }

    public Match teamB(CSGOTeam cSGOTeam) {
        this.teamB = cSGOTeam;
        return this;
    }

    public void setTeamB(CSGOTeam cSGOTeam) {
        this.teamB = cSGOTeam;
    }

    public CSGOTeam getWinner() {
        return winner;
    }

    public Match winner(CSGOTeam cSGOTeam) {
        this.winner = cSGOTeam;
        return this;
    }

    public void setWinner(CSGOTeam cSGOTeam) {
        this.winner = cSGOTeam;
    }

    public Set<Map> getMaps() {
        return maps;
    }

    public Match maps(Set<Map> maps) {
        this.maps = maps;
        return this;
    }

    public Match addMaps(Map map) {
        this.maps.add(map);
        return this;
    }

    public Match removeMaps(Map map) {
        this.maps.remove(map);
        return this;
    }

    public void setMaps(Set<Map> maps) {
        this.maps = maps;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Match match = (Match) o;
        if (match.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), match.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Match{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", numberOfMaps=" + getNumberOfMaps() +
            ", championship='" + getChampionship() + "'" +
            "}";
    }
}
