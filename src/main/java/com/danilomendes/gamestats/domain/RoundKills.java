package com.danilomendes.gamestats.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A RoundKills.
 */
@Entity
@Table(name = "round_kills")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RoundKills implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "no_kill")
    private Long noKill;

    @Column(name = "one_kill")
    private Long oneKill;

    @Column(name = "two_kills")
    private Long twoKills;

    @Column(name = "three_kills")
    private Long threeKills;

    @Column(name = "four_kills")
    private Long fourKills;

    @Column(name = "five_kills")
    private Long fiveKills;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoKill() {
        return noKill;
    }

    public RoundKills noKill(Long noKill) {
        this.noKill = noKill;
        return this;
    }

    public void setNoKill(Long noKill) {
        this.noKill = noKill;
    }

    public Long getOneKill() {
        return oneKill;
    }

    public RoundKills oneKill(Long oneKill) {
        this.oneKill = oneKill;
        return this;
    }

    public void setOneKill(Long oneKill) {
        this.oneKill = oneKill;
    }

    public Long getTwoKills() {
        return twoKills;
    }

    public RoundKills twoKills(Long twoKills) {
        this.twoKills = twoKills;
        return this;
    }

    public void setTwoKills(Long twoKills) {
        this.twoKills = twoKills;
    }

    public Long getThreeKills() {
        return threeKills;
    }

    public RoundKills threeKills(Long threeKills) {
        this.threeKills = threeKills;
        return this;
    }

    public void setThreeKills(Long threeKills) {
        this.threeKills = threeKills;
    }

    public Long getFourKills() {
        return fourKills;
    }

    public RoundKills fourKills(Long fourKills) {
        this.fourKills = fourKills;
        return this;
    }

    public void setFourKills(Long fourKills) {
        this.fourKills = fourKills;
    }

    public Long getFiveKills() {
        return fiveKills;
    }

    public RoundKills fiveKills(Long fiveKills) {
        this.fiveKills = fiveKills;
        return this;
    }

    public void setFiveKills(Long fiveKills) {
        this.fiveKills = fiveKills;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RoundKills roundKills = (RoundKills) o;
        if (roundKills.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), roundKills.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RoundKills{" +
            "id=" + getId() +
            ", noKill=" + getNoKill() +
            ", oneKill=" + getOneKill() +
            ", twoKills=" + getTwoKills() +
            ", threeKills=" + getThreeKills() +
            ", fourKills=" + getFourKills() +
            ", fiveKills=" + getFiveKills() +
            "}";
    }
}
