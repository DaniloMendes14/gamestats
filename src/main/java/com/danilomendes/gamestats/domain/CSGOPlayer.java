package com.danilomendes.gamestats.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A CSGOPlayer.
 */
@Entity
@Table(name = "csgo_player")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CSGOPlayer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private Long age;

    @Column(name = "nick")
    private String nick;

    @Column(name = "rating")
    private Double rating;

    @Column(name = "users_rating")
    private Double usersRating;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @Column(name = "maps_played")
    private Long mapsPlayed;

    @Column(name = "win_loss_ratio")
    private Double winLossRatio;

    @Column(name = "t_win_ratio")
    private Double tWinRatio;

    @Column(name = "ct_win_ratio")
    private Double ctWinRatio;

    @Column(name = "kd_ratio")
    private Double kdRatio;

    @Column(name = "kd_diff")
    private Long kdDiff;

    @Column(name = "headshot_radio")
    private Double headshotRadio;

    @Column(name = "best_weapon")
    private String bestWeapon;

    @Column(name = "twitter_url")
    private String twitterUrl;

    @Column(name = "twitch_url")
    private String twitchUrl;

    @Column(name = "hardware_specs")
    private String hardwareSpecs;

    @Column(name = "total_rounds")
    private Long totalRounds;

    @Column(name = "grenade_dmg_round")
    private Double grenadeDmgRound;

    @Column(name = "kill_round_ratio")
    private Double killRoundRatio;

    @Column(name = "death_round_ratio")
    private Double deathRoundRatio;

    @Column(name = "total_kill")
    private Long totalKill;

    @Column(name = "total_death")
    private Long totalDeath;

    @OneToOne
    @JoinColumn(unique = true)
    private RoundKills roundKills;

    @OneToOne
    @JoinColumn(unique = true)
    private Match lastMatch;

    @OneToOne
    @JoinColumn(unique = true)
    private Country country;

    @ManyToOne
    private CSGOTeam team;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CSGOPlayer name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAge() {
        return age;
    }

    public CSGOPlayer age(Long age) {
        this.age = age;
        return this;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getNick() {
        return nick;
    }

    public CSGOPlayer nick(String nick) {
        this.nick = nick;
        return this;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Double getRating() {
        return rating;
    }

    public CSGOPlayer rating(Double rating) {
        this.rating = rating;
        return this;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Double getUsersRating() {
        return usersRating;
    }

    public CSGOPlayer usersRating(Double usersRating) {
        this.usersRating = usersRating;
        return this;
    }

    public void setUsersRating(Double usersRating) {
        this.usersRating = usersRating;
    }

    public byte[] getImage() {
        return image;
    }

    public CSGOPlayer image(byte[] image) {
        this.image = image;
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public CSGOPlayer imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public Long getMapsPlayed() {
        return mapsPlayed;
    }

    public CSGOPlayer mapsPlayed(Long mapsPlayed) {
        this.mapsPlayed = mapsPlayed;
        return this;
    }

    public void setMapsPlayed(Long mapsPlayed) {
        this.mapsPlayed = mapsPlayed;
    }

    public Double getWinLossRatio() {
        return winLossRatio;
    }

    public CSGOPlayer winLossRatio(Double winLossRatio) {
        this.winLossRatio = winLossRatio;
        return this;
    }

    public void setWinLossRatio(Double winLossRatio) {
        this.winLossRatio = winLossRatio;
    }

    public Double gettWinRatio() {
        return tWinRatio;
    }

    public CSGOPlayer tWinRatio(Double tWinRatio) {
        this.tWinRatio = tWinRatio;
        return this;
    }

    public void settWinRatio(Double tWinRatio) {
        this.tWinRatio = tWinRatio;
    }

    public Double getCtWinRatio() {
        return ctWinRatio;
    }

    public CSGOPlayer ctWinRatio(Double ctWinRatio) {
        this.ctWinRatio = ctWinRatio;
        return this;
    }

    public void setCtWinRatio(Double ctWinRatio) {
        this.ctWinRatio = ctWinRatio;
    }

    public Double getKdRatio() {
        return kdRatio;
    }

    public CSGOPlayer kdRatio(Double kdRatio) {
        this.kdRatio = kdRatio;
        return this;
    }

    public void setKdRatio(Double kdRatio) {
        this.kdRatio = kdRatio;
    }

    public Long getKdDiff() {
        return kdDiff;
    }

    public CSGOPlayer kdDiff(Long kdDiff) {
        this.kdDiff = kdDiff;
        return this;
    }

    public void setKdDiff(Long kdDiff) {
        this.kdDiff = kdDiff;
    }

    public Double getHeadshotRadio() {
        return headshotRadio;
    }

    public CSGOPlayer headshotRadio(Double headshotRadio) {
        this.headshotRadio = headshotRadio;
        return this;
    }

    public void setHeadshotRadio(Double headshotRadio) {
        this.headshotRadio = headshotRadio;
    }

    public String getBestWeapon() {
        return bestWeapon;
    }

    public CSGOPlayer bestWeapon(String bestWeapon) {
        this.bestWeapon = bestWeapon;
        return this;
    }

    public void setBestWeapon(String bestWeapon) {
        this.bestWeapon = bestWeapon;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public CSGOPlayer twitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
        return this;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    public String getTwitchUrl() {
        return twitchUrl;
    }

    public CSGOPlayer twitchUrl(String twitchUrl) {
        this.twitchUrl = twitchUrl;
        return this;
    }

    public void setTwitchUrl(String twitchUrl) {
        this.twitchUrl = twitchUrl;
    }

    public String getHardwareSpecs() {
        return hardwareSpecs;
    }

    public CSGOPlayer hardwareSpecs(String hardwareSpecs) {
        this.hardwareSpecs = hardwareSpecs;
        return this;
    }

    public void setHardwareSpecs(String hardwareSpecs) {
        this.hardwareSpecs = hardwareSpecs;
    }

    public Long getTotalRounds() {
        return totalRounds;
    }

    public CSGOPlayer totalRounds(Long totalRounds) {
        this.totalRounds = totalRounds;
        return this;
    }

    public void setTotalRounds(Long totalRounds) {
        this.totalRounds = totalRounds;
    }

    public Double getGrenadeDmgRound() {
        return grenadeDmgRound;
    }

    public CSGOPlayer grenadeDmgRound(Double grenadeDmgRound) {
        this.grenadeDmgRound = grenadeDmgRound;
        return this;
    }

    public void setGrenadeDmgRound(Double grenadeDmgRound) {
        this.grenadeDmgRound = grenadeDmgRound;
    }

    public Double getKillRoundRatio() {
        return killRoundRatio;
    }

    public CSGOPlayer killRoundRatio(Double killRoundRatio) {
        this.killRoundRatio = killRoundRatio;
        return this;
    }

    public void setKillRoundRatio(Double killRoundRatio) {
        this.killRoundRatio = killRoundRatio;
    }

    public Double getDeathRoundRatio() {
        return deathRoundRatio;
    }

    public CSGOPlayer deathRoundRatio(Double deathRoundRatio) {
        this.deathRoundRatio = deathRoundRatio;
        return this;
    }

    public void setDeathRoundRatio(Double deathRoundRatio) {
        this.deathRoundRatio = deathRoundRatio;
    }

    public RoundKills getRoundKills() {
        return roundKills;
    }

    public CSGOPlayer roundKills(RoundKills roundKills) {
        this.roundKills = roundKills;
        return this;
    }

    public void setRoundKills(RoundKills roundKills) {
        this.roundKills = roundKills;
    }

    public Match getLastMatch() {
        return lastMatch;
    }

    public CSGOPlayer lastMatch(Match match) {
        this.lastMatch = match;
        return this;
    }

    public void setLastMatch(Match match) {
        this.lastMatch = match;
    }

    public Country getCountry() {
        return country;
    }

    public CSGOPlayer country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public CSGOTeam getTeam() {
        return team;
    }

    public CSGOPlayer team(CSGOTeam cSGOTeam) {
        this.team = cSGOTeam;
        return this;
    }

    public void setTeam(CSGOTeam cSGOTeam) {
        this.team = cSGOTeam;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CSGOPlayer cSGOPlayer = (CSGOPlayer) o;
        if (cSGOPlayer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cSGOPlayer.getId());
    }

    public Long getTotalKill() {
        return totalKill;
    }

    public void setTotalKill(Long totalKill) {
        this.totalKill = totalKill;
    }

    public CSGOPlayer totalKill(Long totalKill) {
        this.totalKill = totalKill;
        return this;
    }

    public Long getTotalDeath() {
        return totalDeath;
    }

    public void setTotalDeath(Long totalDeath) {
        this.totalDeath = totalDeath;
    }

    public CSGOPlayer totalDeath(Long totalDeath) {
        this.totalKill = totalKill;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CSGOPlayer{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", age=" + getAge() +
            ", nick='" + getNick() + "'" +
            ", rating=" + getRating() +
            ", usersRating=" + getUsersRating() +
            ", image='" + getImage() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            ", mapsPlayed=" + getMapsPlayed() +
            ", winLossRatio=" + getWinLossRatio() +
            ", tWinRatio=" + gettWinRatio() +
            ", ctWinRatio=" + getCtWinRatio() +
            ", kdRatio=" + getKdRatio() +
            ", kdDiff=" + getKdDiff() +
            ", headshotRadio=" + getHeadshotRadio() +
            ", bestWeapon='" + getBestWeapon() + "'" +
            ", twitterUrl='" + getTwitterUrl() + "'" +
            ", twitchUrl='" + getTwitchUrl() + "'" +
            ", hardwareSpecs='" + getHardwareSpecs() + "'" +
            ", totalRounds=" + getTotalRounds() +
            ", grenadeDmgRound=" + getGrenadeDmgRound() +
            ", killRoundRatio=" + getKillRoundRatio() +
            ", deathRoundRatio=" + getDeathRoundRatio() +
            ", totalKill=" + getTotalKill() +
            ", totalDeath=" + getTotalDeath() +
            "}";
    }
}
