package com.danilomendes.gamestats.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Created by danilomendes on 07/02/18.
 */
public class AppContextUtils implements ApplicationContextAware {

    private static ApplicationContext APP_CONTEXT;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        APP_CONTEXT = applicationContext;
    }

    public static <T> T getBean(Class<T> clazz) {
        return APP_CONTEXT.getBean(clazz);
    }
}
