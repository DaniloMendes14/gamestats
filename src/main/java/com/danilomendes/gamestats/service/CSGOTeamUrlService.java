package com.danilomendes.gamestats.service;

import com.danilomendes.gamestats.domain.CSGOTeamUrl;
import com.danilomendes.gamestats.repository.CSGOTeamUrlRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing CSGOTeamUrl.
 */
@Service
@Transactional
public class CSGOTeamUrlService {

    private final Logger log = LoggerFactory.getLogger(CSGOTeamUrlService.class);

    private final CSGOTeamUrlRepository cSGOTeamUrlRepository;

    public CSGOTeamUrlService(CSGOTeamUrlRepository cSGOTeamUrlRepository) {
        this.cSGOTeamUrlRepository = cSGOTeamUrlRepository;
    }

    /**
     * Save a cSGOTeamUrl.
     *
     * @param cSGOTeamUrl the entity to save
     * @return the persisted entity
     */
    public CSGOTeamUrl save(CSGOTeamUrl cSGOTeamUrl) {
        log.debug("Request to save CSGOTeamUrl : {}", cSGOTeamUrl);
        return cSGOTeamUrlRepository.save(cSGOTeamUrl);
    }

    /**
     * Get all the cSGOTeamUrls.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CSGOTeamUrl> findAll() {
        log.debug("Request to get all CSGOTeamUrls");
        return cSGOTeamUrlRepository.findAll();
    }

    /**
     * Get one cSGOTeamUrl by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public CSGOTeamUrl findOne(Long id) {
        log.debug("Request to get CSGOTeamUrl : {}", id);
        return cSGOTeamUrlRepository.findOne(id);
    }

    /**
     * Delete the cSGOTeamUrl by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CSGOTeamUrl : {}", id);
        cSGOTeamUrlRepository.delete(id);
    }
}
