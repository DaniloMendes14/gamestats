package com.danilomendes.gamestats.service;

import com.danilomendes.gamestats.domain.CSGOPlayer;
import com.danilomendes.gamestats.domain.CSGOPlayerUrl;
import com.danilomendes.gamestats.repository.CSGOPlayerUrlRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by danilomendes on 27/01/18.
 */

@Transactional
@Service
public class CSGOPlayerService {

    private final static Logger log = LoggerFactory.getLogger(CSGOPlayerService.class);

    @Autowired
    private CountryService countryMgr;

    @Autowired
    private CSGOPlayerUrlRepository csgoPlayerUrlRepository;

    public void createPlayerUrl(String name, String statsUrl){
        CSGOPlayerUrl playerUrl = csgoPlayerUrlRepository.findOneByName("name").
            orElseGet(() -> {
                CSGOPlayerUrl player = new CSGOPlayerUrl();
                player.setName(name);
                return player;
            });

        playerUrl.setStatsUrl(statsUrl);
        csgoPlayerUrlRepository.save(playerUrl);

        log.info("Salvo: " + playerUrl.toString());
    }


    public CSGOPlayer getPlayer(String html){
        Document document = Jsoup.parse(html);
        CSGOPlayer player = new CSGOPlayer();

        //set nick
        Elements list = document.getElementsByClass("context-item-name");
        for(Element name: list){
            player.setNick(name.text());
        }

        //set name
        list = document.getElementsByClass("statsPlayerName");
        for(Element name: list){
            player.setName(name.text());
        }

        //set age and rating
        list = document.getElementsByClass("center-column");
        player.setRating(Double.valueOf(list.get(0).getElementsByClass("large-strong").text()));
        player.setAge(Long.valueOf(list.get(1).getElementsByClass("large-strong").text()));
        //set country
        player.setCountry(countryMgr.saveCountry(list.get(2).getElementsByClass("large-strong").text()));


        list = document.getElementsByClass("stats-row");
        player.setTotalKill(Long.valueOf(list.get(0).lastElementSibling().text()));
        player.setHeadshotRadio(Double.valueOf(format(list.get(1).lastElementSibling().text())));
        player.setTotalDeath(Long.valueOf(list.get(2).lastElementSibling().text()));
        player.setKdRatio(Double.valueOf(list.get(3).lastElementSibling().text()));
        player.setGrenadeDmgRound(Double.valueOf(list.get(4).lastElementSibling().text()));
        player.setMapsPlayed(Long.valueOf(list.get(5).lastElementSibling().text()));
        player.setTotalRounds(Long.valueOf(list.get(6).lastElementSibling().text()));
        player.setDeathRoundRatio(Double.valueOf(list.get(9).lastElementSibling().text()));

        log.info(player.toString());

        return null;
    }

    private String format(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == 'x') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

}
