package com.danilomendes.gamestats.service;

import com.danilomendes.gamestats.domain.Country;
import com.danilomendes.gamestats.repository.CountryRepository;
import com.danilomendes.gamestats.web.rest.CountryResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Created by danilomendes on 26/01/18.
 */

@Service
@Transactional
public class CountryService {

    @Autowired
    private CountryRepository countryRepository;

    public Country saveCountry(String countryName){
         return countryRepository.findOneByCountryName(countryName).orElseGet(
            () -> {
                Country country = new Country();
                country.setCountryName(countryName);
                country = countryRepository.save(country);
                return country;
            });
    }
}
