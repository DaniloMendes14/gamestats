package com.danilomendes.gamestats.repository;

import com.danilomendes.gamestats.domain.RoundKills;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RoundKills entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RoundKillsRepository extends JpaRepository<RoundKills, Long> {

}
