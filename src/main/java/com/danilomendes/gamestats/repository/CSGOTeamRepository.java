package com.danilomendes.gamestats.repository;

import com.danilomendes.gamestats.domain.CSGOTeam;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CSGOTeam entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CSGOTeamRepository extends JpaRepository<CSGOTeam, Long> {

}
