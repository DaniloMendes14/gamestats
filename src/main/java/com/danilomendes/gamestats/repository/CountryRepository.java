package com.danilomendes.gamestats.repository;

import com.danilomendes.gamestats.domain.Country;
import com.danilomendes.gamestats.domain.User;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.Optional;


/**
 * Spring Data JPA repository for the Country entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

    Optional<Country> findOneByCountryName(String countryName);

}
