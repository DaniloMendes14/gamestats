package com.danilomendes.gamestats.repository;

import com.danilomendes.gamestats.domain.CSGOPlayerUrl;
import com.danilomendes.gamestats.domain.Country;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.Optional;


/**
 * Spring Data JPA repository for the CSGOPlayerUrl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CSGOPlayerUrlRepository extends JpaRepository<CSGOPlayerUrl, Long> {

    Optional<CSGOPlayerUrl> findOneByName(String name);

}
