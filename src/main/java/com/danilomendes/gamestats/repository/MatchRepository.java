package com.danilomendes.gamestats.repository;

import com.danilomendes.gamestats.domain.Match;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Match entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MatchRepository extends JpaRepository<Match, Long> {
    @Query("select distinct match from Match match left join fetch match.maps")
    List<Match> findAllWithEagerRelationships();

    @Query("select match from Match match left join fetch match.maps where match.id =:id")
    Match findOneWithEagerRelationships(@Param("id") Long id);

}
