package com.danilomendes.gamestats.repository;

import com.danilomendes.gamestats.domain.CSGOTeamUrl;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CSGOTeamUrl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CSGOTeamUrlRepository extends JpaRepository<CSGOTeamUrl, Long> {

}
