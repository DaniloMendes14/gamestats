package com.danilomendes.gamestats.repository;

import com.danilomendes.gamestats.domain.CSGOPlayer;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CSGOPlayer entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CSGOPlayerRepository extends JpaRepository<CSGOPlayer, Long> {

}
