package com.danilomendes.gamestats.webcrawler;

import com.danilomendes.gamestats.service.AppContextUtils;
import com.danilomendes.gamestats.service.CSGOPlayerService;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

/**
 * Created by danilomendes on 29/01/18.
 */
public class CSGOPlayerListCrawler extends WebCrawler{


    private CSGOPlayerService playerServiceMgr;

    private static final Logger log = LoggerFactory.getLogger(CSGOPlayerListCrawler.class);

    private static final Pattern IMAGE_EXTENSIONS = Pattern.compile(".*(\\.(css|js|gif|jpg" + "|png|mp3|mp4|zip|gz))$");

    public CSGOPlayerListCrawler() {
        playerServiceMgr = AppContextUtils.getBean(CSGOPlayerService.class);
    }

    /**
     * You should implement this function to specify whether the given url
     * should be crawled or not (based on your crawling logic).
     */
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        // Ignore the url if it has an extension that matches our defined set of image extensions.
        if (IMAGE_EXTENSIONS.matcher(href).matches()) {
            return false;
        }

        return true;
    }

    /**
     * This function is called when a page is fetched and ready to be processed
     * by your program.
     */
    @Override
    public void visit(Page page) {
        int docId = page.getWebURL().getDocid();
        String url = page.getWebURL().getURL();
        String domain = page.getWebURL().getDomain();
        String path = page.getWebURL().getPath();
        String subDomain = page.getWebURL().getSubDomain();
        String parentUrl = page.getWebURL().getParentUrl();
        String anchor = page.getWebURL().getAnchor();

        log.debug("Docid: {}", docId);
        log.info("URL: {}", url);
        log.debug("Domain: '{}'", domain);
        log.debug("Sub-domain: '{}'", subDomain);
        log.debug("Path: '{}'", path);
        log.debug("Parent page: {}", parentUrl);
        log.debug("Anchor text: {}", anchor);

        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String html = htmlParseData.getHtml();

            if(html != null){
                parseHtml(html);
            }
        }
    }

    public void parseHtml(String html){
        Document document = Jsoup.parse(html);
        Elements list = document.getElementsByTag("td");
        for(Element element: list){
            if(element.hasClass("playerCol")){
                String name = element.lastElementSibling().text();
                String playerUrl = element.lastElementSibling().attr("href");
                playerServiceMgr.createPlayerUrl(name, playerUrl);
            }
        }
    }

}
